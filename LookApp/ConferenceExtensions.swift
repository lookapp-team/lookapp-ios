//
//  ConferenceExtensions.swift
//  LookApp
//
//  Created by xcodvv on 18.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import WebRTC

extension Conference {
    
    func add(delegate: ConferenceDelegate) {
        delegates.add(delegate)
    }
    
    func remove(delegate: ConferenceDelegate) {
        delegates.remove(delegate)
    }
    
}

extension Conference: OrientationInfoDelegate {
    
    func didChangeOrientation(_ orientation: Orientation) {
        
    }
    
}

extension Conference: SignalerDelegate {
    
    func didAddPeer(peer: PeerConnection) {
        peer.setLogLevel(.error)
        peer.delegates.add(self)
    }
    
    func makeLocalVideoTransceiver(for peer: PeerConnection) {
        let transceiver = peer.rtc.addTransceiver(of: .video)
        transceiver.direction = .sendRecv
        transceiver.sender.track = PeerConnectonFactory.sharedFactory.videoTrack(with: peer.videoSource, trackId: "video:\(UUID())")
        //CustomVideoCapturer.shared.frameDelegates.add(peer.videoSource)
        switch(mixerLayout.state) {
            
        case .single, .simple:
            CustomVideoCapturer.shared.delegates.add(peer.videoSource)

        default:
            CustomVideoCapturer.shared.delegates.add(peer.videoSource)
        }
    }
    
    // host should create an offer
    
    func shouldCreateOffer(for peer: PeerConnection) {
        print("shouldCreateOffer")
        makeLocalVideoTransceiver(for: peer)
    }
    
    // guest handle offser

    func willCreateAnswer(for peer: PeerConnection) {
        print("willCreateAnswer")
        for transciever in peer.rtc.transceivers {
            transciever.direction = .sendRecv
            transciever.sender.track = PeerConnectonFactory.sharedFactory.videoTrack(with: peer.videoSource, trackId: "video:\(UUID())")
            switch(mixerLayout.state) {
                
            case .single, .simple:
                CustomVideoCapturer.shared.delegates.add(peer.videoSource)

            default:
                CustomVideoCapturer.shared.delegates.add(peer.videoSource)
            }
        }
    }
    
}

extension Conference: PeerConnectionDelegate {
    
    func makeVideoReceiver(with transceiver: RTCRtpTransceiver, peer: PeerConnection) {
        let videoReceiver = CustomVideoReceiver(transceiver, peer: peer, mtkView: renderer.mtkView)
        videoReceiver.drawable.then { _ in
            self.videoProvider[peer.endpoint] = videoReceiver
            self.viewLayout.sprites.append(videoReceiver.sprite)
            self.updateLayout()
        }
    }
    
    func didStartReceiving(on transceiver: RTCRtpTransceiver, peer: PeerConnection) {
        print("[peer] got transceiver", "#\(transceiver.mid)", transceiver.direction.toString(), transceiver.mediaType.toString())
        switch transceiver.mediaType {
        case .video:
            makeVideoReceiver(with: transceiver, peer: peer)
        case .audio:
            return
        case .data:
            return
        @unknown default:
            return
        }
    }
    
    func didAddReceiver(rtpReceiver: RTCRtpReceiver, streams mediaStreams: [RTCMediaStream], peer: PeerConnection) {
        print("didAddReceiver")
    }
    
    func didRemoveReceiver(rtpReceiver: RTCRtpReceiver, peer: PeerConnection) {
        
    }
    
    func didChangeIceConnectionState(newState: RTCIceConnectionState, peer: PeerConnection) {
        switch newState {
        case .connected:
            peer.setLogLevel(.none)
        default:
            return
        }
    }
    
}
