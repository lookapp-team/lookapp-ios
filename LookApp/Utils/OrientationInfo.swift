//
//  OrientationInfo.swift
//  LookApp
//
//  Created by xcodvv on 16.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import UIKit

enum Orientation {
    case portrait
    case landscape
}

protocol OrientationInfoDelegate {
    func didChangeOrientation(_ orientation: Orientation)
}

final class OrientationInfo: ObservableObject {
    
    @Published var orientation: Orientation
    @Published var peerOrientation = Dictionary<String, Orientation>()
    var delegate: OrientationInfoDelegate?
    private var _observer: NSObjectProtocol?
    
    init() {
        // fairly arbitrary starting value for 'flat' orientations
        if UIDevice.current.orientation.isLandscape {
            self.orientation = .landscape
        }
        else {
            self.orientation = .portrait
        }
        
        // unowned self because we unregister before self becomes invalid
        _observer = NotificationCenter.default.addObserver(forName: UIDevice.orientationDidChangeNotification, object: nil, queue: nil) { [unowned self] note in
            guard let device = note.object as? UIDevice else {
                return
            }
            if device.orientation.isPortrait {
                if self.orientation != .portrait {
                    self.orientation = .portrait
                    if let dg = self.delegate {
                        dg.didChangeOrientation(self.orientation)
                    }
                }
            }
            else if device.orientation.isLandscape {
                if self.orientation != .landscape {
                    self.orientation = .landscape
                    if let dg = self.delegate {
                        dg.didChangeOrientation(self.orientation)
                    }
                }
            }
        }
    }
    
    deinit {
        if let observer = _observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
}
