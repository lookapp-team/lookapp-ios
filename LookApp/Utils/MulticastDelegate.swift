//
//  MulticastDelegate.swift
//  LookApp
//
//  Created by xcodvv on 16.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation

class MulticastDelegate <T> {
    private let delegates: NSHashTable<AnyObject> = NSHashTable.weakObjects()

    func add(_ delegate: T) {
        for oneDelegate in delegates.allObjects {
            if oneDelegate === delegate as AnyObject {
                return
            }
        }
        delegates.add(delegate as AnyObject)
    }

    func remove(_ delegate: T) {
        for oneDelegate in delegates.allObjects.reversed() {
            if oneDelegate === delegate as AnyObject {
                delegates.remove(oneDelegate)
            }
        }
    }

    func removeAll() {
        delegates.removeAllObjects()
    }
    
    func invoke(_ invocation: (T) -> ()) {
        for delegate in delegates.allObjects.reversed() {
            invocation(delegate as! T)
        }
    }
}

func += <T: AnyObject> (left: MulticastDelegate<T>, right: T) {
    left.add(right)
}

func -= <T: AnyObject> (left: MulticastDelegate<T>, right: T) {
    left.remove(right)
}
