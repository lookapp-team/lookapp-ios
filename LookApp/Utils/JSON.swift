//
//  JSON.swift
//  LookApp
//
//  Created by xcodvv on 16.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation

typealias RawObject = Dictionary<String, Any>

class JSON: NSObject
{
    static func parse(_ data: Data, _ options: JSONSerialization.ReadingOptions) throws ->JSON {
        do {
            let data = try JSONSerialization.jsonObject(with: data, options: options)
            return JSON(data as! RawObject)
        } catch {
            throw error
        }
    }
    
    static func stringify(_ data: RawObject, _ options: JSONSerialization.WritingOptions) throws ->String {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: options)
            return String(decoding: jsonData, as: UTF8.self)
        } catch {
            throw error
        }
    }
    
    var data: RawObject
    
    init(_ withData: RawObject) {
        data = withData
    }
    
    func contains(_ key: String)->Bool {
        if let _ = data[key] {
            return true
        }
        return false
    }
    
    func rawObject(_ key: String)->RawObject? {
        if let val = data[key] {
            return val as? RawObject
        }
        return nil
    }
    
    func toJSON(_ key: String)->JSON? {
        if let val = data[key] {
            return JSON(val as! RawObject)
        }
        return nil
    }
    
    func key(_ key: String)->String? {
        return toString(key)
    }
    
    func toString(_ key: String)->String? {
        if let val = data[key] {
            if let val = val as? String {
                return val
            }
            return "\(val)"
        }
        return nil
    }
    
    func toString(_ key: String, _ defaultValue: String)->String {
        if let val = data[key] {
            return val as! String
        }
        return defaultValue
    }
    
    func toInt32(_ key: String)->Int32? {
        if let val = data[key] {
            return val as! Int32
        }
        return 0
    }
    
    func toInt32(_ key: String, _ defaultValue: Int32)->Int32 {
        if let val = data[key] {
            return val as! Int32
        }
        return defaultValue
    }
    
}
