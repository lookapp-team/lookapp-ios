//
//  ConferenceLayout.swift
//  LookApp
//
//  Created by xcodvv on 06.09.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import MetalKit

enum ConferenceLayoutState {
    case none
    case single
    case simple
}

class ConferenceLayout: SpritesCollection {
    
    var localVideoSprite: VideoSprite? = nil
    
    var sprites = [Sprite]()
    
    var state = ConferenceLayoutState.single
    
    var renderer: SequentialRenderer!

    
    init(_ renderer: SequentialRenderer) {
        self.renderer = renderer
    }
    
    func updateLayout() {
        guard let localVideoSprite = localVideoSprite else { return }
        
        //let viewSize = max(renderer.drawableSize.x, renderer.drawableSize.y)
        let count = Float(sprites.count)
        
        var dir = vector_float2(1, 1)
        switch UIDevice.current.orientation {
        case .portrait, .portraitUpsideDown, .faceDown, .faceUp:
            dir.y = count
        case .landscapeLeft, .landscapeRight, .unknown:
            dir.x = count
        default:
            break
        }
        
        let baseSize = vector_float2(Float(renderer.size.width) / dir.x, Float(renderer.size.height) / dir.y)
        
        localVideoSprite.size = baseSize
    
        switch sprites.count {
        
        case 2:
            for (_, peerVideo) in Conference.shared.videoProvider {
                peerVideo.sprite.size = baseSize
                if UIDevice.current.orientation.isLandscape {
                    if peerVideo.sprite.orientation.isLandscape {
                        let width = (baseSize.x / baseSize.y) * peerVideo.sprite.frameSize.y
                        let ratio = width / peerVideo.sprite.frameSize.x
                        peerVideo.sprite.frameUV = vector_float2(ratio, 1)
                        peerVideo.sprite.frameOrigin = vector_float2(0, 0)
                    } else {
                        let height = (baseSize.y / baseSize.x) * peerVideo.sprite.frameSize.x
                        let ratio =  height / peerVideo.sprite.frameSize.y
                        peerVideo.sprite.frameUV = vector_float2(ratio, 1)
                        peerVideo.sprite.frameOrigin = vector_float2(0, 0)
                    }
                } else {
                    if peerVideo.sprite.orientation.isLandscape {
                        let width = (baseSize.x / baseSize.y) * peerVideo.sprite.frameSize.y
                        let ratio = width / peerVideo.sprite.frameSize.x
                        peerVideo.sprite.frameUV = vector_float2(ratio, 1)
                        peerVideo.sprite.frameOrigin = vector_float2(0, 0)
                    } else {
                        let height = (baseSize.y / baseSize.x) * peerVideo.sprite.frameSize.x
                        let ratio =  height / peerVideo.sprite.frameSize.y
                        peerVideo.sprite.frameUV = vector_float2(ratio, 1)
                        peerVideo.sprite.frameOrigin = vector_float2(0, 0)
                    }
                }
                peerVideo.sprite.update()
            }
            if UIDevice.current.orientation.isLandscape {
                sprites[0].pivot = vector_float2(1, 0.5)
                sprites[1].pivot = vector_float2(0, 0.5)
                let size = (baseSize.x / baseSize.y) * localVideoSprite.frameSize.y
                let ratio = size / localVideoSprite.frameSize.x
                print("landscape \(size) \(ratio)")
                localVideoSprite.frameUV = vector_float2(ratio, 1)
                localVideoSprite.frameOrigin = vector_float2(0, 0)
            } else {
                sprites[0].pivot = vector_float2(0.5, 0)
                sprites[1].pivot = vector_float2(0.5, 1)
                let size = (baseSize.y / baseSize.x) * localVideoSprite.frameSize.x
                let ratio = size / localVideoSprite.frameSize.y
                print("portrait \(size) \(ratio)")
                localVideoSprite.frameUV = vector_float2(ratio, 1)
                localVideoSprite.frameOrigin = vector_float2(0, 0)
            }
            //localVideoSprite.frameUV = vector_float2(0.5, localVideoSprite.frameScale)
            //localVideoSprite.frameOrigin = vector_float2(0, 0)
            localVideoSprite.update()
            
        default:
            localVideoSprite.pivot = vector_float2(0.5, 0.5)
            localVideoSprite.frameOrigin = vector_float2(0, 0)
            localVideoSprite.frameUV = vector_float2(1, localVideoSprite.frameScale)
            localVideoSprite.update()
        }
    }
    
}
