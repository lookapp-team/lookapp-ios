//
//  CustomRenderView.swift
//  LookApp
//
//  Created by xcodvv on 18.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import UIKit
import SwiftUI
import MetalKit
import Promises

struct CustomRenderView: UIViewRepresentable {

    var renderer: CustomRender
    
    public func makeUIView(context: Context) -> MTKView {
        return renderer.mtkView
    }
    
    public func updateUIView(_ uiView: MTKView, context: Context) {
        
    }
}


struct OrientationView: UIViewControllerRepresentable {
    
    let orientationController = OrientationController()
    
    func makeUIViewController(context: Context) -> OrientationController {
        return orientationController
    }
    
    func updateUIViewController(_ uiViewController: OrientationController, context: Context) {
        
    }
}

typealias OrientationCallback = (_ size: CGSize, _ transition: Promise<CGSize>)->Void

class OrientationController: UIViewController {
    
    private static var _callback = [OrientationCallback?]()
    
    static func drawable(_ callback: @escaping OrientationCallback) {
        _callback.append(callback)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        let size = CGSize(width: size.width*UIScreen.main.nativeScale, height: size.height*UIScreen.main.nativeScale)
        let promise = Promise<CGSize>.pending()
        for callback in OrientationController._callback {
            if let callback = callback {
                callback(size, promise)
            }
        }
        coordinator.animate(alongsideTransition: nil) { _ in
            promise.fulfill(size)
        }
    }
    
}
