//
//  Shaders.metal
//  LookApp
//
//  Created by xcodvv on 19.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct Uniforms {
    float4x4 modelViewProjectionMatrix;
    float4x4 normalMatrix;
    float4 color;
};

struct InstanceUniforms {
    float2 size;
    float4x4 transform;
    float4 color;
    float2x4 uvs;
    ushort texIndex;
};

struct VertexIn {
    float3 position [[attribute(0)]];
    float3 normal   [[attribute(1)]];
    float2 texCoords [[attribute(2)]];
};

struct VertexOut {
    float4 position [[position]];
    float3 normal;
    float4 color;
    float2 texCoords;
    ushort texIndex;
};

struct FragmentOut {
    float4 viewTarget [[ color(0) ]];
};

struct YpFragmentOut {
    float planeTarget [[ color(0) ]];
};


vertex VertexOut vertex_main(VertexIn in [[stage_in]],
                             constant Uniforms &unforms [[buffer(1)]],
                             constant InstanceUniforms *instanceUniforms [[buffer(2)]],
                             ushort vertexId [[vertex_id]],
                             ushort instanceId [[instance_id]])
{
    InstanceUniforms instance = instanceUniforms[instanceId];
    VertexOut out;
    
    float4 position(in.position, 1);
    float4 normal(in.normal, 0);
    float4 size(instance.size.x/2, instance.size.y/2, 1, 1);
    
    out.position = (unforms.modelViewProjectionMatrix * instance.transform) * (position * size);
    out.normal = (unforms.normalMatrix * normal).xyz;
    out.color = unforms.color * instance.color;
    
    out.texCoords = float2(instance.uvs.columns[0][vertexId], instance.uvs.columns[1][vertexId]);
    
    out.texIndex = instance.texIndex; //instanceId; //
    
    return out;
}

fragment FragmentOut fragment_main(VertexOut in [[stage_in]],
                            texture2d<float, access::sample> textureY [[texture(0)]],
                            texture2d<float, access::sample> textureCbCr [[texture(1)]],
                            texture2d<float, access::sample> textureY2 [[texture(2)]],
                            texture2d<float, access::sample> textureCbCr2 [[texture(3)]],
                            sampler baseColorSampler [[sampler(0)]])
{
    FragmentOut out;
    float y;
    float2 uv;
    if (in.texIndex == 0) {
        y = textureY.sample(baseColorSampler, in.texCoords).r;
        uv = textureCbCr.sample(baseColorSampler, in.texCoords).rg;
    } else if (in.texIndex == 1) {
        y = textureY2.sample(baseColorSampler, in.texCoords).r;
        uv = textureCbCr2.sample(baseColorSampler, in.texCoords).rg;
    } else {
        y = textureY.sample(baseColorSampler, in.texCoords).r;
        uv = textureCbCr.sample(baseColorSampler, in.texCoords).rg;
    }
    uv = uv - float2(0.5, 0.5);
    float4 c = float4(y + 1.403 * uv.y, y - 0.344 * uv.x - 0.714 * uv.y, y + 1.770 * uv.x, 1.0);
    out.viewTarget = c * in.color;
    return out;
}

fragment YpFragmentOut fragment_with_Yplane(VertexOut in [[stage_in]],
                            texture2d<float, access::sample> textureY [[texture(0)]],
                            texture2d<float, access::sample> textureCbCr [[texture(1)]],
                            texture2d<float, access::sample> textureY2 [[texture(2)]],
                            texture2d<float, access::sample> textureCbCr2 [[texture(3)]],
                            sampler baseColorSampler [[sampler(0)]])
{
    YpFragmentOut out;
    float y;
    if (in.texIndex == 0) {
        y = textureY.sample(baseColorSampler, in.texCoords).r;
    } else if (in.texIndex == 1) {
        y = textureY2.sample(baseColorSampler, in.texCoords).r;
    } else {
        y = textureY.sample(baseColorSampler, in.texCoords).r;
    }
    out.planeTarget = y;
    return out;
}
