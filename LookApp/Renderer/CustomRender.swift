//
//  CustomRender.swift
//  LookApp
//
//  Created by xcodvv on 18.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import MetalKit
import Promises

class CustomRender: NSObject {
    
    var mtkView: MTKView!
    var device: MTLDevice!
    var commandQueue: MTLCommandQueue!
    var drawable = Promise<CustomRender>.pending()
    var drawableSize: vector_float2 { vector_float2(Float(mtkView.drawableSize.width), Float(mtkView.drawableSize.height)) }
    
    //let preferredWideSize = CGSize(width: 1280, height: 720)
    //let preferredSize = CGSize(width: 1024, height: 768)
    
    //private(set) var scaleFactor: CGFloat = 1
    //private(set) var nativeRatio: CGFloat = 16/9
    private(set) var size = UIScreen.main.nativeBounds.size//CGSize(width: 0, height: 0)
    private(set) var isReady = false
    
    //let renderedSemaphor = DispatchSemaphore(value: 0)
    let frameSemaphor = DispatchSemaphore(value: 3)
    let maxFramesInFlight = 3
    private(set) var currentFrameIndex = 0
    
    
    lazy var defaultLibrary: MTLLibrary = {
        return device.makeDefaultLibrary()!
    }()
    
    
    override init() {
        super.init()
        
        device = MTLCreateSystemDefaultDevice()
        commandQueue = device.makeCommandQueue()
        
        mtkView = MTKView()
        mtkView.delegate = self
        mtkView.isPaused = true
        mtkView.enableSetNeedsDisplay = false
        mtkView.framebufferOnly = true
        mtkView.device = device
        mtkView.colorPixelFormat = .bgra8Unorm
        mtkView.depthStencilPixelFormat = .invalid
        mtkView.preferredFramesPerSecond = 30
        mtkView.sampleCount = 1
        mtkView.presentsWithTransaction = false
        
        print("[screen] size", UIScreen.main.nativeBounds.size, "scale", UIScreen.main.nativeScale)
    }
    
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        if !isReady {
            setViewportSize(size)
            drawable.fulfill(self)
            isReady = true
        }
    }
    
    func setViewportSize(_ value: CGSize) {
        size = value
        print("[render] size", size, "scale", mtkView.contentScaleFactor, UIDevice.current.orientation.isLandscape ? "landscape" : "portrait")
    }
    
    internal func nextFrame() {
        currentFrameIndex = (currentFrameIndex+1)%maxFramesInFlight
    }
    
    func draw(in view: MTKView) {
        
    }

}
