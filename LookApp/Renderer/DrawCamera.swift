
import simd

class DrawCamera {
    var fieldOfView: Float = 60.0
    var nearZ: Float = 0.1
    var farZ: Float = 100.0
    
    func projectionMatrix(aspectRatio: Float) -> float4x4 {
        return float4x4(perspectiveProjectionRHFovY: radians_from_degrees(fieldOfView),
                        aspectRatio: aspectRatio,
                        nearZ: nearZ,
                        farZ: farZ)
    }
    
    class func makeOrthographicMatrix(_ left: Float, _ right: Float, _ bottom: Float, _ top: Float, near: Float, far: Float) -> float4x4 {
        let lr = 1 / (left - right);
        let bt = 1 / (bottom - top);
        let nf = 1 / (near - far);
        return float4x4(
            [-2 * lr, 0, 0, 0 ],
            [ 0, -2 * bt, 0, 0 ],
            [ 0, 0, 2 * nf, 0],
            [(left + right) * lr, (top + bottom) * bt, (far + near) * nf, 1]
        )
    }
}
