//
//  Sprite.swift
//  WebRTCTest
//
//  Created by xcodvv on 31.07.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import MetalKit

class Sprite: Equatable, CustomDebugStringConvertible {
    
    static func == (d: Sprite, b: Sprite) -> Bool {
        return d.id == b.id
    }
    
    let id = UUID()
    var name: String? = nil
    var active = true
    var tags = [String]()
    
    var size = vector_float2(64, 64)
    var pivot = vector_float2(0.5, 0.5)
    var scale = vector_float2(1, 1)
    var position = vector_float3(0, 0, 0)
    var rotation = Float(0)
    var color = vector_float4(1, 1, 1, 1)
    var textureIndex: ushort = 0
    var uvs = float2x4(rows: [ vector_float2(0, 0),
                               vector_float2(1, 0),
                               vector_float2(0, 1),
                               vector_float2(1, 1)])
    
    var debugDescription: String { return "<Scprite>: \(name ?? "unnamed")" }
    
    init() {
        awake()
    }
    
    init(position: vector_float2) {
        self.position = vector_float3(position.x, position.y, 0)
        awake()
    }
    
    init(size: vector_float2) {
        self.size = vector_float2(size.x, size.y)
        awake()
    }
    
    init(size: vector_float2, position: vector_float2) {
        self.size = vector_float2(size.x, size.y)
        self.position = vector_float3(position.x, position.y, 0)
        awake()
    }
    
    func awake() {
        
    }
    
    func set(for vertexIndex: Int, uv texCoord: vector_float2) {
        if vertexIndex >= 0 && vertexIndex < 4 {
            uvs[0][vertexIndex] = texCoord.x
            uvs[1][vertexIndex] = texCoord.y
        }
    }
    
}
