//
//  DrawView.swift
//  LookApp
//
//  Created by xcodvv on 22.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import MetalKit

class DrawView: SequentialRenderPass {
    
    static let FragmentFunctionName = "fragment_main"
    
     
    override func draw(_ commandBuffer: MTLCommandBuffer) {
        guard let renderPipelineState = renderPipelineState else { return }
        
        guard let renderPassDescriptor = renderPassDescriptor else { return }
        guard let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor) else { return }
        
        commandEncoder.setRenderPipelineState(renderPipelineState)
        
        for handler in renderer.renderCommandEncoder {
            handler(commandEncoder, self)
        }
        
        for handler in renderHandlers {
            handler(commandEncoder, self)
        }
        
        commandEncoder.endEncoding()
    }
    
}
