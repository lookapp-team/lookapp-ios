//
//  VideoCapturer.swift
//  LookApp
//
//  Created by xcodvv on 18.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import WebRTC

class CustomVideoCapturer: NSObject {
    
    static let shared = CustomVideoCapturer()
    
    static let dummyCapturer = RTCVideoCapturer()
    
    static func startCapture(device: AVCaptureDevice, capturer: RTCCameraVideoCapturer) -> AVCaptureDevice.Format {
        let formats = RTCCameraVideoCapturer.supportedFormats(for: device)
        var acceptFormats = [AVCaptureDevice.Format]()
        var fps = 25;
        var dim: CMVideoDimensions
        for fmt in formats {
            dim = CMVideoFormatDescriptionGetDimensions(fmt.formatDescription)
            if let frameRate = fmt.videoSupportedFrameRateRanges.last {
                if (frameRate.maxFrameRate <= 30 && (dim.width >= 640 || dim.height >= 360)) {
                    acceptFormats.append(fmt)
                    let frameRate = Int(frameRate.maxFrameRate)
                    if frameRate > fps {
                        fps = frameRate
                    }
                    break
                }
            }
                
        }
        capturer.startCapture(with: device, format:acceptFormats.last!, fps:min(fps, 30))
        return acceptFormats.last!
    }
    
    static var frontCaptureDevice: AVCaptureDevice? {
        let videoDevices = RTCCameraVideoCapturer.captureDevices()
        for dev in videoDevices {
            if dev.position == .front {
                return dev
            }
        }
        return nil
    }
    
    static var backCaptureDevice: AVCaptureDevice? {
        let videoDevices = RTCCameraVideoCapturer.captureDevices()
        for dev in videoDevices {
            if dev.position == .back && dev.deviceType == .builtInTelephotoCamera {
                return dev
            }
        }
        return nil
    }
    
    let dispatchFrame = DispatchQueue.init(label: "lookapp.framesQueue", qos: .userInteractive)
    
    let delegates = MulticastDelegate<RTCVideoCapturerDelegate>()
    
    let frameDelegates = MulticastDelegate<RTCVideoCapturerDelegate>()

    lazy var capturer: RTCCameraVideoCapturer = {
        let capturer = RTCCameraVideoCapturer()
        capturer.delegate = self
        return capturer
    }()
}

extension CustomVideoCapturer: RTCVideoCapturerDelegate {
    
    func capturer(_ capturer: RTCVideoCapturer, didCapture frame: RTCVideoFrame) {
        delegates.invoke { delegate in
            delegate.capturer(capturer, didCapture: frame)
        }
    }
    
    func frame(_ capturer: RTCVideoCapturer, didCapture frame: RTCVideoFrame) {
        dispatchFrame.async {
            self.frameDelegates.invoke { delegate in
                delegate.capturer(capturer, didCapture: frame)
            }
        }
    }
}
