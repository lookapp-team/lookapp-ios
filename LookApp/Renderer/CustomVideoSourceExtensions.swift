//
//  CustomVideoSourceExtensions.swift
//  LookApp
//
//  Created by xcodvv on 19.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import WebRTC

extension CustomVideoSource: RTCVideoCapturerDelegate {
    
    func capturer(_ capturer: RTCVideoCapturer, didCapture frame: RTCVideoFrame) {
        CustomVideoSource.framesQueue.async {
            self.applyFrame(frame)
            self.nextFrame()
        }
    }

}
