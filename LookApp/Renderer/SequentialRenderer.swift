//
//  SequentialRenderer.swift
//  LookApp
//
//  Created by xcodvv on 19.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import MetalKit

typealias CompleteHandler = ()->Void

typealias RenderCommandEncoderCallback = (_ commandEncoder: MTLRenderCommandEncoder, _ drawPass: SequentialRenderPass)->Void


class SequentialRenderPass {
    
    let id = UUID()
    var renderer: SequentialRenderer!
    var active = true
    
    private(set) var renderPassing = [SequentialRenderPass]()
    private(set) var renderHandlers = [RenderCommandEncoderCallback]()
    
    var filterTags = [String]()
    
    var drawableSize: vector_float2 {
        getDrawableSize()
    }
    
    var renderPassDescriptor: MTLRenderPassDescriptor? {
        makePassDescriptor()
    }
    
    lazy var renderPipelineDescriptor: MTLRenderPipelineDescriptor = {
        makePipelineDescriptor()
    }()
    
    lazy var renderPipelineState: MTLRenderPipelineState? = {
        makePipelineState()
    }()
    
    init(_ renderer: SequentialRenderer) {
        self.renderer = renderer
        self.renderer.add(self)
    }
    
    func getDrawableSize() -> vector_float2 {
        return renderer.drawableSize
    }
    
    func makePipelineState() -> MTLRenderPipelineState? {
        do {
            return try? renderer.device.makeRenderPipelineState(descriptor: renderPipelineDescriptor)
        }
    }
    
    func makePipelineDescriptor() -> MTLRenderPipelineDescriptor {
        let descriptor = MTLRenderPipelineDescriptor()
        descriptor.sampleCount = 1
        descriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
        descriptor.depthAttachmentPixelFormat = .invalid
        descriptor.fragmentFunction = renderer.defaultLibrary.makeFunction(name: DrawView.FragmentFunctionName)
        return descriptor
    }
    
    func makePassDescriptor() -> MTLRenderPassDescriptor? {
        return renderer.mtkView.currentRenderPassDescriptor
    }
    
    func setViewportSize(_ value: CGSize) {
        
    }
    
    func awake() {
        
    }
    
    func draw(_ commandBuffer: MTLCommandBuffer) {
        
    }
    
    func add(_ pass: SequentialRenderPass) {
        renderPassing.append(pass)
    }
    
    func add(_ handler: @escaping RenderCommandEncoderCallback) {
        renderHandlers.append(handler)
    }
}

class SequentialRenderer: CustomRender {
    
    private var renderPassing = [SequentialRenderPass]()
    private var completeHandler: CompleteHandler? = nil
    private(set) var renderCommandEncoder = [RenderCommandEncoderCallback]()
    private var ns = Dictionary<String, UUID>()
    
    func add(_ pass: SequentialRenderPass) {
        renderPassing.append(pass)
        pass.awake()
    }
    
    func add(_ pass: SequentialRenderPass, name: String) {
        ns[name] = pass.id
        add(pass)
    }
    
    func add(_ commandEncoderCallback: @escaping RenderCommandEncoderCallback) {
        renderCommandEncoder.append(commandEncoderCallback)
    }
    
    func awake() {
//        for pass in renderPassing {
//            pass.awake()
//        }
    }
    
    func complete(_ callback: @escaping CompleteHandler) {
        completeHandler = callback
    }
    
    override func draw(in view: MTKView) {
        guard let drawable = mtkView.currentDrawable else { return }
        guard let commandBuffer = commandQueue.makeCommandBuffer() else { return }
        
        self.frameSemaphor.wait()
        
        nextFrame()
        
        for pass in renderPassing {
            pass.draw(commandBuffer)
        }
        
        commandBuffer.addCompletedHandler { _ in
            if let handler = self.completeHandler {
                handler()
            }
            self.frameSemaphor.signal()
            //self.renderedSemaphor.signal()
        }
        
        commandBuffer.present(drawable)
        commandBuffer.commit()
    }
    
    override func setViewportSize(_ value: CGSize) {
        super.setViewportSize(value)
        for pass in renderPassing {
            pass.setViewportSize(value)
        }
    }
}
