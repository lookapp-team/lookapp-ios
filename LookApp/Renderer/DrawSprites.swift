//
//  DrawSprites.swift
//  LookApp
//
//  Created by xcodvv on 19.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import MetalKit
import simd

struct Uniforms {
    var modelViewProjectionMatrix: float4x4
    var normalMatrix: float4x4
    var color: vector_float4
    var texOffset: ushort
}

struct InstanceUniforms {
    var size: vector_float2
    var transform: float4x4
    var color: vector_float4
    var uvs: float2x4
    var texIndex: ushort
}

protocol SpritesCollection: class {
    
    var sprites: [Sprite] { get }
    
}

class DrawSprites {
    
    static let VertexFunctionName = "vertex_main"
    
    static let UniformsBufferIndex = 1
    static let UniformsBufferLength = 65_536
    static let UniformsSize = alignUp(size: MemoryLayout<Uniforms>.size, align: MemoryLayout<Uniforms>.alignment)
    
    static let InstanceUniformsBufferIndex = 2
    static let InstanceUniformsBufferLength = 65_536
    static let InstanceUniformsSize = alignUp(size: MemoryLayout<InstanceUniforms>.size, align: MemoryLayout<InstanceUniforms>.alignment)
    
    var drawPass: SequentialRenderPass!
    var renderer: SequentialRenderer { drawPass.renderer }
    
    var globalTransform = matrix_identity_float4x4
    
    weak var collection: SpritesCollection? //= Dictionary<UUID, Sprite>()
    
    //private(set) var frameIndex = 0
    //private(set) var frameBufferOffset = 0
    //let maxFramesInFlight = 3
    
    private var instanceBuffers = [MTLBuffer]()
    private(set) var meshAllocator: MTKMeshBufferAllocator!
    private(set) var mesh: MTKMesh!
    
    private var uniformsBuffers = [MTLBuffer]()
    private var worldMatrix = matrix_identity_float4x4
    private var viewMatrix = float4x4.init(translationBy: SIMD3<Float>(0, 0, -100))
    private var projectionMatrix = float4x4.init()
    
    
    lazy var vertexDescriptor: MDLVertexDescriptor = {
        let descriptor = MDLVertexDescriptor()
        
        descriptor.layouts[0] = MDLVertexBufferLayout(stride: MemoryLayout<Float>.size * (3 + 3))

        descriptor.attributes[0] = MDLVertexAttribute(name: MDLVertexAttributePosition,
                                                            format: .float3,
                                                            offset: 0,
                                                            bufferIndex: 0)

        descriptor.attributes[1] = MDLVertexAttribute(name: MDLVertexAttributeNormal,
                                                            format: .float3,
                                                            offset: MemoryLayout<Float>.size * (3),
                                                            bufferIndex: 0)
        return descriptor
    }()
    
    
    init(_ sprites: SpritesCollection, _ pass: SequentialRenderPass) {
        collection = sprites
        drawPass = pass
        setViewportSize(renderer.mtkView.drawableSize)
        makePipelineDescriptor()
        makeUniformsBuffer()
        makeInstanceBuffer()
        makeMesh()
        drawPass.add(draw)
        OrientationController.drawable { (size, transition) in
            self.setViewportSize(size)
        }
    }
    
    func setViewportSize(_ value: CGSize) {
        let nx = Float(value.width)/2
        let ny = Float(value.height)/2
        projectionMatrix = DrawCamera.makeOrthographicMatrix(-nx, nx, -ny, ny, near: -100, far: 100)
    }
    
    private func makePipelineDescriptor() {
        let vertexFunction = renderer.defaultLibrary.makeFunction(name: DrawSprites.VertexFunctionName)

        let descriptor = drawPass.renderPipelineDescriptor
        descriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(vertexDescriptor)
        descriptor.vertexFunction = vertexFunction
        descriptor.vertexBuffers[0].mutability = .immutable
        descriptor.vertexBuffers[1].mutability = .immutable
        descriptor.vertexBuffers[2].mutability = .immutable
    }
    
    private func makeMesh() {
        meshAllocator = meshAllocator ?? MTKMeshBufferAllocator(device: renderer.device)
        
        let mdlMesh = MDLMesh.init(planeWithExtent: vector_float3(2, 2, 1),
                                   segments: vector_uint2(1, 1),
                                   geometryType: MDLGeometryType.quads,
                                   allocator: meshAllocator)
        
        mdlMesh.vertexDescriptor = vertexDescriptor
        
        mesh = try! MTKMesh(mesh: mdlMesh, device: renderer.device)
    }
    
    private func makeInstanceBuffer() {
        for _ in 0..<renderer.maxFramesInFlight {
            guard let buf = renderer.device.makeBuffer(length: DrawSprites.InstanceUniformsBufferLength,
                                                       options: [ .storageModeShared, .hazardTrackingModeUntracked ]) else { continue }
            instanceBuffers.append(buf)
        }
    }
    
    private func makeUniformsBuffer() {
        var constants = Uniforms(modelViewProjectionMatrix: projectionMatrix * viewMatrix * worldMatrix,
                                 normalMatrix: viewMatrix * worldMatrix,
                                 color: vector_float4(1, 1, 1, 1),
                                 texOffset: 0)
        for _ in 0..<renderer.maxFramesInFlight {
            guard let buf = renderer.device.makeBuffer(length: MemoryLayout<Uniforms>.size, options: [.storageModeShared]) else { continue }
            memcpy(buf.contents(), &constants, MemoryLayout<Uniforms>.size)
            uniformsBuffers.append(buf)
        }
    }
    
    func updateUniforms(_ frameIndex: Int) {
        if frameIndex >= 0 && frameIndex < uniformsBuffers.count {
            var constants = Uniforms(modelViewProjectionMatrix: projectionMatrix * viewMatrix * worldMatrix,
                                     normalMatrix: viewMatrix * worldMatrix,
                                     color: vector_float4(1, 1, 1, 1),
                                     texOffset: 0)
            
            memcpy(uniformsBuffers[frameIndex].contents(), &constants, MemoryLayout<Uniforms>.size)
        }
    }
    
    func draw(_ commandEncoder: MTLRenderCommandEncoder, _ renderPass: SequentialRenderPass) {
        
        updateUniforms(renderer.currentFrameIndex)
        let uniformsBuffer = uniformsBuffers[renderer.currentFrameIndex]
        
        let instanceBuffer = instanceBuffers[renderer.currentFrameIndex]
        
        var totalCount = 0
        
        guard let collection = collection else { return }
        for sprite in collection.sprites {
            if !sprite.active { continue }
            
            if renderPass.filterTags.count > 0 {
                if let _ = renderPass.filterTags.first(where: { tag in return sprite.tags.firstIndex(of: tag) != nil }) {  //tag
                    
                } else {
                    //continue
                }
            }
            
            let localTransform = globalTransform
                               * matrix_float4x4(rotationAroundAxis: vector_float3(0, 0, 1), by: sprite.rotation)
                               * matrix_float4x4(translationBy: vector_float3(sprite.size/2 - sprite.size * sprite.pivot, 0))
            
            let transform = matrix_identity_float4x4
                          * matrix_float4x4(diagonal: vector_float4(sprite.scale.x, sprite.scale.y, 1, 1))
                           * matrix_float4x4(translationBy: sprite.position)
                          * localTransform
            
            var instance = InstanceUniforms(size: sprite.size,
                                            transform: transform,
                                            color: sprite.color,
                                            uvs: sprite.uvs,
                                            texIndex: sprite.textureIndex)
            
            memcpy(instanceBuffer.contents()+totalCount*DrawSprites.InstanceUniformsSize,
                   &instance, DrawSprites.InstanceUniformsSize)
            totalCount += 1
        }
        
         
        if totalCount == 0 { return }
        
        if mesh.vertexBuffers.count > 0 {
            let vertexBuffer = mesh.vertexBuffers[0]
            commandEncoder.setVertexBuffer(vertexBuffer.buffer, offset: 0, index: 0)
        }
        
        commandEncoder.setVertexBuffer(instanceBuffer, offset: 0, index: DrawSprites.InstanceUniformsBufferIndex)
        
        commandEncoder.setVertexBuffer(uniformsBuffer, offset: 0, index: DrawSprites.UniformsBufferIndex)
        
        commandEncoder.setFragmentSamplerState(samplerState, index: 0)
        
        for submesh in mesh.submeshes {
            commandEncoder.setTriangleFillMode(.fill)
            commandEncoder.drawIndexedPrimitives(type: submesh.primitiveType,
                                                 indexCount: submesh.indexCount,
                                                 indexType: submesh.indexType,
                                                 indexBuffer: submesh.indexBuffer.buffer,
                                                 indexBufferOffset: submesh.indexBuffer.offset,
                                                 instanceCount: totalCount)
        }
        
    }
    
    lazy var samplerState: MTLSamplerState = {
        let samplerDescriptor = MTLSamplerDescriptor()
        samplerDescriptor.normalizedCoordinates = true
        samplerDescriptor.minFilter = .linear
        samplerDescriptor.magFilter = .linear
        samplerDescriptor.mipFilter = .notMipmapped
        samplerDescriptor.sAddressMode = .clampToEdge
        samplerDescriptor.tAddressMode = .clampToEdge
        return renderer.device.makeSamplerState(descriptor: samplerDescriptor)!
    }()
}
