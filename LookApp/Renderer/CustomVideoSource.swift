//
//  CustomVideoSource.swift
//  LookApp
//
//  Created by xcodvv on 19.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import MetalKit
import WebRTC
import Promises

class CustomVideoSource: NSObject {
    
    static let framesQueue = DispatchQueue(label: "io.lookapp.framesQueue")
    
    var mtkView: MTKView!
    var device: MTLDevice!
    var textureCache: CVMetalTextureCache!
    var size = CGSize(width: 640, height: 360)
    
    var framePixelBuffer: CVPixelBuffer?
    var framePTS: Int64 = 0
    var frameTexture = [MTLTexture?]()
    let maxFramesInFlight = 3
    var currentFrameIndex = 0
    var nextFrameIndex = 0
    
    let drawable = Promise<CustomVideoSource>.pending()
    
    lazy var frameTextureAttributes = [
        kCVMetalTextureUsage: [ MTLTextureUsage.shaderRead ],
        kCVMetalTextureStorageMode: MTLStorageMode.shared,
    ] as CFDictionary
    
    lazy var frameTextureCacheAttributes = [
        kCVMetalTextureCacheMaximumTextureAgeKey: 0,
    ] as CFDictionary
    
    
    init(_ view: MTKView) {
        mtkView = view
        device = view.device
        super.init()
        if CVMetalTextureCacheCreate(kCFAllocatorDefault, frameTextureCacheAttributes, device, frameTextureAttributes, &textureCache) != kCVReturnSuccess {
            print("Fail to create Metal capture frame texture cache")
        }
    }
    
    func nextFrame() {
        currentFrameIndex = nextFrameIndex
        nextFrameIndex = (currentFrameIndex+1)%maxFramesInFlight
    }
    
    func applyFrame(_ frame: RTCVideoFrame) {
        guard let frameData = frame.buffer as? RTCCVPixelBuffer else {
            print("Unxpected frame buffer", type(of: frame.buffer))
            return
        }
        let pixelBuffer = frameData.pixelBuffer
        let lumaWidth = CVPixelBufferGetWidthOfPlane(pixelBuffer, 0)
        let lumaHeight = CVPixelBufferGetHeightOfPlane(pixelBuffer, 0)
        var lumaTexture: MTLTexture?
        var chromaTexture: MTLTexture?
        var outTexture: CVMetalTexture?
        if CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                     textureCache, pixelBuffer, nil, .r8Unorm,
                                                     lumaWidth,
                                                     lumaHeight,
                                                     0,
                                                     &outTexture) == kCVReturnSuccess {
            lumaTexture = CVMetalTextureGetTexture(outTexture!)
        }
        outTexture = nil
        if CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                     textureCache, pixelBuffer, nil, .rg8Unorm,
                                                     CVPixelBufferGetWidthOfPlane(pixelBuffer, 1),
                                                     CVPixelBufferGetHeightOfPlane(pixelBuffer, 1),
                                                     1,
                                                     &outTexture) == kCVReturnSuccess {
            chromaTexture = CVMetalTextureGetTexture(outTexture!)
        }
        outTexture = nil
        if lumaTexture != nil && chromaTexture != nil {
            size.width = CGFloat(lumaWidth)
            size.height = CGFloat(lumaHeight)
            framePixelBuffer = pixelBuffer
            framePTS = frame.timeStampNs
            if frameTexture.count < maxFramesInFlight*2 {
                frameTexture.append(contentsOf: [lumaTexture, chromaTexture])
                drawable.fulfill(self)
            } else {
                let offset = nextFrameIndex * 2
                frameTexture[offset] = nil
                frameTexture[offset+1] = nil
                frameTexture[offset] = lumaTexture
                frameTexture[offset+1] = chromaTexture
            }
            lumaTexture = nil
            chromaTexture = nil
        }
    }
    
}
