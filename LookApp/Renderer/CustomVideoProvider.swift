//
//  CustomVideoProvider.swift
//  LookApp
//
//  Created by xcodvv on 01.09.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import WebRTC
import MetalKit

class CustomVideoReceiver: CustomVideoSource {
    
    let transceiver: RTCRtpTransceiver
    
    let peer: PeerConnection
    
    let sprite = VideoSprite()
    
    var track: RTCVideoTrack?
    
    init(_ rtpTransceiver: RTCRtpTransceiver, peer: PeerConnection, mtkView: MTKView) {
        self.transceiver = rtpTransceiver
        self.peer = peer
        super.init(mtkView)
        if let track = transceiver.receiver.track {
            self.track = track as? RTCVideoTrack
        }
        if let track = track {
            track.add(self)
        }
    }
    
}

extension CustomVideoReceiver: RTCVideoRenderer {
    
    func setSize(_ size: CGSize) {
        self.size = size
        sprite.size = vector_float2(Float(size.width), Float(size.height))
    }
    
    func renderFrame(_ frame: RTCVideoFrame?) {
        guard let frame = frame else { return }
        sprite.setOrientation(getOrientation(by: frame.rotation))
        applyFrame(frame)
        CustomVideoSource.framesQueue.async {
            self.nextFrame()
        }
    }
    
    func getOrientation(by rotation: RTCVideoRotation) -> UIDeviceOrientation  {
        switch rotation {
        case ._0:
            return .portrait
        case ._90:
            return .landscapeRight
        case ._180:
            return .landscapeLeft
        case ._270:
            return .portraitUpsideDown
        default:
            return .portrait
        }
    }
    
}
