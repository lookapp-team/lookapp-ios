//
//  CustomVideoProvider.swift
//  LookApp
//
//  Created by xcodvv on 01.09.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import WebRTC
import MetalKit

class CustomVideoReceiver: CustomVideoSource {
    
    let transceiver: RTCRtpTransceiver
    
    let peer: PeerConnection
    
    let sprite = VideoSprite()
    
    var track: RTCVideoTrack?
    
    
    init(_ rtpTransceiver: RTCRtpTransceiver, peer: PeerConnection, mtkView: MTKView) {
        self.transceiver = rtpTransceiver
        self.peer = peer
        super.init(mtkView)
        if let track = transceiver.receiver.track {
            self.track = track as? RTCVideoTrack
        }
        if let track = track {
            track.add(self)
        }
    }
    
}

extension CustomVideoReceiver: RTCVideoRenderer {
    
    func setSize(_ size: CGSize) {
        print("[peer]", peer.endpoint, "video", transceiver.mid, "size", size)
        self.size = size
        sprite.frameSize = vector_float2(Float(size.width), Float(size.height))
    }
    
    func renderFrame(_ frame: RTCVideoFrame?) {
        guard let frame = frame else { return }
        let orientation = getOrientation(by: frame.rotation)
        if sprite.orientation.rawValue != orientation.rawValue {
            sprite.setOrientation(orientation)
            Conference.shared.updateLayout()
        }
        CustomVideoSource.framesQueue.async {
            self.applyFrame(frame)
            self.nextFrame()
        }
    }
    
    func getOrientation(by rotation: RTCVideoRotation) -> UIDeviceOrientation  {
        switch rotation {
        case ._0:
            return .landscapeRight
        case ._90:
            return .portrait
        case ._180:
            return .landscapeLeft
        case ._270:
            return .portraitUpsideDown
        default:
            return .portrait
        }
    }
    
}
