//
//  DrawLumaChannel.swift
//  LookApp
//
//  Created by xcodvv on 19.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import MetalKit

class DrawLumaChannel: SequentialRenderPass {
    
    static let FragmentFunctionName = "fragment_with_Yplane"
    
    let maxFrameRenderBuffers = 3
    var frameRenderIndex = 0
    var frameRenderTextureCache: CVMetalTextureCache!
    var frameRenderPixelBuffer = [CVPixelBuffer]()
    var frameRenderTexture = [MTLTexture]()
    
    
    override func awake() {
        
        if CVMetalTextureCacheCreate(kCFAllocatorDefault, renderTextureCacheAttributes, renderer.device, renderTextureAttributes, &frameRenderTextureCache) != kCVReturnSuccess {
            print("Fail to create Metal frame render texture cache")
            return
        }
        
        for _ in 0 ..< maxFrameRenderBuffers  {
            makeRenderBuffer420YpCrCb()
        }
    }
    
    override func draw(_ commandBuffer: MTLCommandBuffer) {
        
        guard let renderPipelineState = renderPipelineState else { return }
        guard let renderPassDescriptor = renderPassDescriptor else { return }
        
        let frameIndex = frameRenderIndex
        let frameOffset = frameIndex * 2
        renderPassDescriptor.colorAttachments[0].texture = frameRenderTexture[frameOffset]
        
        guard let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor) else { return }
        commandEncoder.setRenderPipelineState(renderPipelineState)
        
        for handler in renderer.renderCommandEncoder {
            handler(commandEncoder, self)
        }
        
        for handler in renderHandlers {
            handler(commandEncoder, self)
        }
        
        commandEncoder.endEncoding()
    }
    
    override func getDrawableSize() -> vector_float2 {
        return vector_float2(Float(renderTargetDescriptor.width), Float(renderTargetDescriptor.height))
    }
    
    override func makePassDescriptor() -> MTLRenderPassDescriptor {
        let pass = MTLRenderPassDescriptor()
        pass.colorAttachments[0].clearColor = MTLClearColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 1.0)
        pass.colorAttachments[0].loadAction = .clear
        pass.colorAttachments[0].storeAction = .store
        return pass
    }
    
    override func makePipelineDescriptor() -> MTLRenderPipelineDescriptor {
        let pipeline = MTLRenderPipelineDescriptor()
        pipeline.sampleCount = 1
        pipeline.colorAttachments[0].pixelFormat = .r8Unorm
        pipeline.depthAttachmentPixelFormat = .invalid
        pipeline.fragmentFunction = renderer.defaultLibrary.makeFunction(name: DrawLumaChannel.FragmentFunctionName)
        return pipeline
    }
    
    lazy var renderTargetDescriptor: MTLTextureDescriptor = {
        let textureDescriptor = MTLTextureDescriptor()
        textureDescriptor.textureType = MTLTextureType.type2D
        textureDescriptor.width = 480//Int(size.width)/2
        textureDescriptor.height = 360//Int(size.height)/2
        textureDescriptor.pixelFormat = .bgra8Unorm
        textureDescriptor.storageMode = .shared
        textureDescriptor.usage = [ .renderTarget, .shaderWrite ]
        return textureDescriptor
    }()
    
    func makeRenderBuffer420YpCrCb() {
        var buffer: CVPixelBuffer!
        if CVPixelBufferCreate(kCFAllocatorDefault,
                               renderTargetDescriptor.width,
                               renderTargetDescriptor.height,
                               kCVPixelFormatType_420YpCbCr8BiPlanarFullRange,
                               pixelBufferOptions,
                               &buffer) == kCVReturnSuccess {
            var outTexture: CVMetalTexture?
            let attr = [
                kCVMetalTextureUsage: [ MTLTextureUsage.shaderWrite, MTLTextureUsage.renderTarget ],
                kCVMetalTextureStorageMode: MTLStorageMode.shared,
            ] as CFDictionary
            if CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                         frameRenderTextureCache, buffer, attr, .r8Unorm,
                                                         CVPixelBufferGetWidth(buffer),
                                                         CVPixelBufferGetHeight(buffer), 0,
                                                         &outTexture) == kCVReturnSuccess {
                if let tex = CVMetalTextureGetTexture(outTexture!) {
                    frameRenderTexture.append(tex)
                }
                outTexture = nil
            }
            if CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                         frameRenderTextureCache, buffer, attr, .rg8Unorm,
                                                         CVPixelBufferGetWidth(buffer)/2,
                                                         CVPixelBufferGetHeight(buffer)/2, 1,
                                                         &outTexture) == kCVReturnSuccess {
                if let tex = CVMetalTextureGetTexture(outTexture!) {
                    frameRenderTexture.append(tex)
                }
                outTexture = nil
            }
            frameRenderPixelBuffer.append(buffer)
        }
    }
    
    lazy var pixelBufferOptions: CFDictionary = {
        return [
            kCVPixelBufferMetalCompatibilityKey: true,
            kCVPixelBufferCGImageCompatibilityKey: true,
            kCVPixelBufferCGBitmapContextCompatibilityKey: true,
            kCVPixelBufferIOSurfaceCoreAnimationCompatibilityKey: true,
        ] as CFDictionary;
    }()
    
    lazy var renderTextureAttributes = [
        kCVMetalTextureUsage: [ MTLTextureUsage.shaderRead, MTLTextureUsage.shaderWrite, MTLTextureUsage.renderTarget ],
        kCVMetalTextureStorageMode: MTLStorageMode.shared,
    ] as CFDictionary
    
    lazy var renderTextureCacheAttributes = [
        kCVMetalTextureCacheMaximumTextureAgeKey: 0,
    ] as CFDictionary
    
}
