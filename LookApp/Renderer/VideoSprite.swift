//
//  VideoSprite.swift
//  LookApp
//
//  Created by xcodvv on 29.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import MetalKit


class VideoSprite: Sprite {
    
    var frameSize = vector_float2(480, 360)
    
    var frameRatio: Float {
        return frameSize.x/frameSize.y
    }
    
    var frameScale: Float {
        return frameSize.y/frameSize.x
    }
    
    var frameUV = vector_float2(1, 1)
    
    var frameOrigin = vector_float2(0, 0)
    
    var orientation = UIDeviceOrientation.portrait
    
    
    override func awake() {
        
    }
    
    func setScale(orientation: UIDeviceOrientation) {
        scale = vector_float2(1, 1)
        switch orientation {
        case .landscapeLeft, .landscapeRight:
            //scale.x = frameRatio
            break
        default:
            //scale.y = frameRatio
            break
        }
    }
    
    func setOrientation(_ orientation: UIDeviceOrientation) {
        self.orientation = orientation
        update()
    }
    
    func update() {
        let origin = vector_float2(frameOrigin.x, frameOrigin.y)
        let uv = vector_float2(origin.x + frameUV.x, origin.y + frameUV.y)
        switch orientation {
        case .portrait, .faceUp:
            uvs[0] = vector_float4(uv.x, uv.x, origin.x, origin.x)
            uvs[1] = vector_float4(origin.y, uv.y, origin.y, uv.y)
            break
        case .landscapeLeft:
            uvs[0] = vector_float4(origin.x, uv.x, origin.x, uv.x)
            uvs[1] = vector_float4(origin.y, origin.y, uv.y, uv.y)
            break
        case .landscapeRight:
            uvs[0] = vector_float4(uv.x, origin.x, uv.x, origin.x)
            uvs[1] = vector_float4(uv.y, uv.y, origin.y, origin.y)
            break
        case .portraitUpsideDown, .faceDown:
            uvs[0] = vector_float4(origin.x, origin.x, uv.x, uv.x)
            uvs[1] = vector_float4(uv.y, origin.y, uv.y, origin.y)
            break
        default:
            uvs[0] = vector_float4(uv.x, uv.x, origin.x, origin.x)
            uvs[1] = vector_float4(origin.y, uv.y, origin.y, uv.y)
            break
        }
    }
}
