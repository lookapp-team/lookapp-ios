//
//  RTCChat.swift
//  LookApp
//
//  Created by xcodvv on 16.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import Promises
import WebRTC

enum ConferenceState {
    case new
    case join
    case open
    case broadcast
    case interactive
}

enum ConferenceRole {
    case host
    case speaker
    case spectator
}

protocol ConferenceDelegate {
    
    func didChangeState(newValue: ConferenceState)
    
}

class Conference: NSObject, ObservableObject {
    
    static let shared = Conference()

    
    @Published var readyState = ConferenceState.new {
        didSet {
            switch(readyState) {
            default:
                UIApplication.shared.isIdleTimerDisabled = true
                break;
            }
            delegates.invoke { delegate in
                delegate.didChangeState(newValue: readyState)
            }
        }
    }
    
    
    var endpoint = "ws://souz.local:3333"
    
    let delegates = MulticastDelegate<ConferenceDelegate>()
    let orientationInfo = OrientationInfo()
    
    let semaphore = DispatchSemaphore(value: 0)
    let renderer = SequentialRenderer()
    
    let cameraDevice = CustomVideoCapturer.frontCaptureDevice!
    //let cameraCapturer = CustomVideoCapturer.shared
    var cameraSource: CustomVideoSource!
    
    var videoProvider = Dictionary<String, CustomVideoReceiver>()
    
    var viewPass: DrawView!
    var mixerLumaPass: DrawLumaChannel!
    //var mixerChromaPass: DrawChromaChannel!
    
    var viewLayout: ConferenceLayout!
    var mixerLayout: ConferenceLayout!
    
    let signaler = Signaler()
    let bootstrap = Bootstrap()
    var inviteUrl: URL?
    var role = ConferenceRole.host
    
    
    override init() {
        super.init()
        
        orientationInfo.delegate = self
        
        signaler.delegates.add(self)
        signaler.setChannel(bootstrap.channel)
        
        renderer.drawable.then { _ in
            self.awake()
        }
        
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    func startVideoCapture() {
        DispatchQueue.global(qos: .userInteractive).async {
            CustomVideoCapturer.shared.startCapture(device: self.cameraDevice)
        }
    }
    
    func makeVideoSource() {
        cameraSource = CustomVideoSource(renderer.mtkView)
    }
    
    
    func awake() {
        makeVideoSource()
        startVideoCapture()
        
        renderer.awake()
         
        makeRenderer()
        
        defer {
            updateLayout()
        }
        
        viewLayout.localVideoSprite = VideoSprite()
        viewLayout.localVideoSprite!.setOrientation(UIDevice.current.orientation)
        viewLayout.sprites.append(viewLayout.localVideoSprite!)
        
        OrientationController.drawable { (size, transition) in
            guard self.renderer.isReady else {  return }
            self.renderer.setViewportSize(size)
            if let localVideoSprite = self.viewLayout.localVideoSprite {
                if UIDevice.current.orientation.isLandscape {
                    let frameSize = vector_float2(Float(CustomVideoCapturer.shared.format!.formatDescription.dimensions.width),
                                                  Float(CustomVideoCapturer.shared.format!.formatDescription.dimensions.height))
                    localVideoSprite.frameSize = frameSize
                } else {
                    let frameSize = vector_float2(Float(CustomVideoCapturer.shared.format!.formatDescription.dimensions.height),
                                                  Float(CustomVideoCapturer.shared.format!.formatDescription.dimensions.width))
                    localVideoSprite.frameSize = frameSize
                }
                localVideoSprite.setOrientation(UIDevice.current.orientation)
            }
            self.updateLayout()
        }
        
        cameraSource.drawable.then { _ in
            print("[capture] format", CustomVideoCapturer.shared.format!)
            if UIDevice.current.orientation.isLandscape {
                let frameSize = vector_float2(Float(CustomVideoCapturer.shared.format!.formatDescription.dimensions.width),
                                              Float(CustomVideoCapturer.shared.format!.formatDescription.dimensions.height))
                self.viewLayout.localVideoSprite!.frameSize = frameSize
            } else {
                let frameSize = vector_float2(Float(CustomVideoCapturer.shared.format!.formatDescription.dimensions.height),
                                              Float(CustomVideoCapturer.shared.format!.formatDescription.dimensions.width))
                self.viewLayout.localVideoSprite!.frameSize = frameSize
            }
            self.renderer.mtkView.isPaused = false
        }
    }
    
    func makeRenderer() {
        viewLayout = ConferenceLayout(renderer)
        mixerLayout = ConferenceLayout(renderer)
        
        viewPass = DrawView(renderer)
        viewPass.add(passVideoTextures)
        viewPass.add(passPeersTextures)
        let _ = DrawSprites(viewLayout, viewPass)
        
        mixerLumaPass = DrawLumaChannel(renderer)
        //mixerLumaPass.add(passVideoTextures)
        //mixerLumaPass.add(passPeersTextures)
        let _ = DrawSprites(mixerLayout, mixerLumaPass)
    }
    
    
    func passMixerTextures(_ commandEncoder: MTLRenderCommandEncoder, _ drawPass: SequentialRenderPass) {
        
    }
    
    func passPeersTextures(_ commandEncoder: MTLRenderCommandEncoder, _ drawPass: SequentialRenderPass) {
        var textures = [MTLTexture?]()
        CustomVideoSource.framesQueue.sync {
            var n: ushort = 1;
            for (_, videoProvider) in self.videoProvider {
                let offset = videoProvider.currentFrameIndex*2
                textures.append(videoProvider.frameTexture[offset])
                textures.append(videoProvider.frameTexture[offset+1])
                videoProvider.sprite.textureIndex = n
                n += 1
            }
        }
        for n in 0..<textures.count {
            commandEncoder.setFragmentTexture(textures[n], index: n + 2)
        }
    }
    
    func passVideoTextures(_ commandEncoder: MTLRenderCommandEncoder, _ drawPass: SequentialRenderPass) {
        var textures: [MTLTexture?]? = nil
        var offset = 0
        CustomVideoSource.framesQueue.sync {
            textures = self.cameraSource.frameTexture
            offset = self.cameraSource.currentFrameIndex*2
        }
        let offsetLocalVideoTexture = 2
        if let textures = textures {
            for n in 0..<offsetLocalVideoTexture {
                commandEncoder.setFragmentTexture(textures[offset+n], index: n)
            }
        }
    }
    
    
    func updateLayout() {
        
        switch(videoProvider.count) {
            
        case 0:
            viewLayout.state = .single
            mixerLayout.state = .none
            
        case 1:
            viewLayout.state = .simple
            mixerLayout.state = .none
            
        case 2:
            //viewLayout.state = .quad
            break
            
        default:
            break
        }
        
        switch(role) {
        case .host:
            updateHostLayout()
        case .speaker:
            updateSpeakerLayout()
        case .spectator:
            updateSpectatorLayout()
        }
    }
    
    func updateHostLayout() {
        viewPass.active = true
        
        switch(mixerLayout.state) {
            
        case .single, .simple:
            mixerLumaPass.active = false
            //mixerChromaPass.active = false
            CustomVideoCapturer.shared.frameDelegates.remove(cameraSource)
            CustomVideoCapturer.shared.delegates.add(cameraSource)
            viewLayout.updateLayout()

        default:
            mixerLumaPass.active = false
            //mixerChromaPass.active = false
            CustomVideoCapturer.shared.frameDelegates.remove(cameraSource)
            CustomVideoCapturer.shared.delegates.add(cameraSource)
            viewLayout.updateLayout()
        }
    }
    
    func updateSpeakerLayout() {
        viewPass.active = true
        
        switch(mixerLayout.state) {
            
        case .single, .simple:
            mixerLumaPass.active = false
            //mixerChromaPass.active = false
            CustomVideoCapturer.shared.frameDelegates.remove(cameraSource)
            CustomVideoCapturer.shared.delegates.add(cameraSource)
            viewLayout.updateLayout()

        default:
            mixerLumaPass.active = false
            //mixerChromaPass.active = false
            CustomVideoCapturer.shared.frameDelegates.remove(cameraSource)
            CustomVideoCapturer.shared.delegates.add(cameraSource)
            viewLayout.updateLayout()
        }
    }
    
    func updateSpectatorLayout() {
        
    }
    
    
    func open() {
        role = .host
        
        bootstrap.open(endpoint, "playground") //zk1.openmvc.ru:3334
        
        bootstrap.ready.then { _ in
            self.readyState = .open
            let _ = self.signaler.createProducer("420")
        }
        
        bootstrap.ready.catch { _ in
            self.readyState = .new
        }
    }
    
    func join(to url: URL) {
        role = .speaker
        
        bootstrap.open(endpoint, "playground")
        
        bootstrap.ready.then { _ in
            self.readyState = .open
            let _ = self.signaler.subscribe("420")
        }
        
        bootstrap.ready.catch { _ in
            self.readyState = .join
        }
    }
}
