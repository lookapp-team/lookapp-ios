//
//  JoinConferenceView.swift
//  LookApp
//
//  Created by xcodvv on 30.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import SwiftUI

struct JoinConferenceView: View {
    
    @EnvironmentObject var conference: Conference
    
    @State private var enabled = true
    @State private var pending = false
    @State private var alertUnreachable = false
    
    var buttonColor: Color {
        return enabled && !pending ? .blue : .gray
    }
    
    var body: some View {
        VStack(alignment:.center) {
            Spacer()
            
            Button(action: self.join) {
                Text("join")
                    .padding()
                    .foregroundColor(Color.white)
                    .background(buttonColor)
            }
            .cornerRadius(10)
            .disabled(!enabled || pending)
            
            if pending {
                Spacer()
                ActivityIndicator(shouldAnimate: $pending)
            }
            
            Spacer()
            
            Button(action: { self.conference.readyState = .new }, label: { Text("start test") } )
                .disabled(!enabled || pending)
            
            Spacer()
        }
        .alert(isPresented: $alertUnreachable) {
            Alert(title: Text("endpoint error"), message: Text("endpoint unreachable"),
                  dismissButton: .default(Text("OK")))
        }
    }
    
    func join() {
        guard let url = conference.inviteUrl else { return }
        pending = true
        enabled = false
        
        conference.join(to: url)
        
        conference.bootstrap.ready.then { _ in
            self.pending = false
            self.enabled = false
        }
        
        conference.bootstrap.ready.catch { err in
            self.pending = false
            self.enabled = true
            switch(err) {
            case SignalerError.endpointUnreachable:
                self.alertUnreachable = true
                break;
            default:
                break;
            }
        }
    }
}

struct JoinConferenceView_Previews: PreviewProvider {
    static var previews: some View {
        JoinConferenceView()
    }
}
