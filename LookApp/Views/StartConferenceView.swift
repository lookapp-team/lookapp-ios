//
//  StartConferenceView.swift
//  LookApp
//
//  Created by xcodvv on 16.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import SwiftUI

struct StartConferenceView: View {
    
    @EnvironmentObject var conference: Conference
    
    @State private var enabled = true
    @State private var pending = false
    @State private var alertUnreachable = false
    
    var buttonColor: Color {
        return enabled && !pending ? .blue : .gray
    }
    
    var body: some View {
        VStack(alignment:.center) {
            Spacer()
            
            Button(action: self.start) {
                Text("start")
                    .padding()
                    .foregroundColor(Color.white)
                    .background(buttonColor)
            }
            .cornerRadius(10)
            .disabled(!enabled || pending)
            
            if pending {
                Spacer()
                ActivityIndicator(shouldAnimate: $pending)
            }
            
            Spacer()
            
            Button(action: { self.join(to: "lookapp://420") }, label: { Text("join test") } )
                .disabled(!enabled || pending)
            
            Spacer()
        }
        .alert(isPresented: $alertUnreachable) {
            Alert(title: Text("endpoint error"), message: Text("endpoint unreachable"),
                  dismissButton: .default(Text("OK")))
        }
    }
    
    func start() {
        pending = true
        enabled = false
        
        conference.open()
        
        conference.bootstrap.ready.then { _ in
            self.pending = false
            self.enabled = false
        }
        
        conference.bootstrap.ready.catch { err in
            self.pending = false
            self.enabled = true
            switch(err) {
            case SignalerError.endpointUnreachable:
                self.alertUnreachable = true
                break;
            default:
                break;
            }
        }
    }
    
    func join(to url: String) {
        if let url = URL(string: url) {
            pending = true
            enabled = false
            
            conference.join(to: url)
            //UIApplication.shared.open(url)
            
            conference.bootstrap.ready.then { _ in
                self.pending = false
                self.enabled = false
            }
            
            conference.bootstrap.ready.catch { err in
                self.pending = false
                self.enabled = true
                switch(err) {
                case SignalerError.endpointUnreachable:
                    self.alertUnreachable = true
                    break;
                default:
                    break;
                }
            }
        }
    }
}


struct StartConferenceView_Previews: PreviewProvider {
    static var previews: some View {
        StartConferenceView()
    }
}
