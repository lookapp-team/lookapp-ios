//
//  ContentView.swift
//  LookApp
//
//  Created by xcodvv on 15.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var conference: Conference
    
    @State var orientation = UIDevice.current.orientation

    let orientationChanged = NotificationCenter.default.publisher(for: UIDevice.orientationDidChangeNotification)
        .makeConnectable()
        .autoconnect()
    
    var body: some View {
        ZStack {
            if self.conference.readyState == .new {
                StartConferenceView()
                    .environmentObject(conference)
            } else if self.conference.readyState == .join {
                JoinConferenceView()
                    .environmentObject(conference)
            //} else if self.conference.readyState == .opened {
                //ConferenceSettingsView()
                  //  .environmentObject(conference)
                    //.transition(.move(edge: .trailing))
            } else {
                CustomRenderView(renderer: conference.renderer)
            }
        }
        .edgesIgnoringSafeArea(.all)
        .preferredColorScheme(.light)
        .onReceive(orientationChanged) { _ in
            self.orientation = UIDevice.current.orientation
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
