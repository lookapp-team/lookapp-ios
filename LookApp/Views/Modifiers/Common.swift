//
//  Common.swift
//  LookApp
//
//  Created by xcodvv on 16.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

struct PrimaryButton: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .cornerRadius(10)
    }
}
