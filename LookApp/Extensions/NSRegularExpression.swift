//
//  NSRegularExpression.swift
//  LookApp
//
//  Created by xcodvv on 16.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation

extension NSRegularExpression {
    convenience init(_ pattern: String) {
        do {
            try self.init(pattern: pattern)
        } catch {
            preconditionFailure("Illegal regular expression: \(pattern).")
        }
    }
}

extension NSRegularExpression {
    func test(_ string: String) -> Bool {
        let range = NSRange(location: 0, length: string.utf16.count)
        return firstMatch(in: string, options: [], range: range) != nil
    }
    
    func first(_ string: String) ->String? {
        let q = matches(string)
        return q.count > 0 ? q[0] : nil
    }
    
    func matches(_ string: String) -> [String] {
        let range = NSRange(location: 0, length: string.utf16.count)
        let mm = matches(in: string, options: [], range: range)
        var q = [String]()
        for m in mm {
            let str = (string as NSString).substring(with: m.range)
            q.append(str)
        }
        return q
    }
    
    func matches(_ string: String, _ callback: (String)->String?) -> [String] {
        let range = NSRange(location: 0, length: string.utf16.count)
        let mm = matches(in: string, options: [], range: range)
        var q = [String]()
        for m in mm {
            let str = (string as NSString).substring(with: m.range)
            if let p = callback(str) {
                q.append(p)
            }
        }
        return q
    }
}
