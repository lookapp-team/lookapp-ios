//
//  WKWebView.swift
//  LookApp
//
//  Created by xcodvv on 16.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import WebKit

extension WKWebView {

  func setScrollEnabled(_ enabled: Bool) {
    self.scrollView.isScrollEnabled = enabled
    self.scrollView.panGestureRecognizer.isEnabled = enabled
    self.scrollView.bounces = enabled

    for subview in self.subviews {
        if let subview = subview as? UIScrollView {
            subview.isScrollEnabled = enabled
            subview.bounces = enabled
            subview.panGestureRecognizer.isEnabled = enabled
        }

        for subScrollView in subview.subviews {
            if type(of: subScrollView) == NSClassFromString("WKContentView")! {
                guard let gr = subScrollView.gestureRecognizers else {
                    continue
                }
                for gesture in gr {
                    subScrollView.removeGestureRecognizer(gesture)
                }
            }
        }
    }
  }
}
