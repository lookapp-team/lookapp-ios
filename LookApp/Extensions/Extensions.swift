//
//  Extensions.swift
//  WebRTCTest
//
//  Created by xcodvv on 15.07.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import WebRTC

// Returns a size of the 'inSize' aligned to 'align' as long as align is a power of 2
func alignUp(size: Int, align: Int) -> Int {
    #if DEBUG
    precondition(((align-1) & align) == 0, "Align must be a power of two")
    #endif

    let alignmentMask = align - 1

    return (size + alignmentMask) & ~alignmentMask
}

extension String {
    static func ~= (lhs: String, rhs: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: rhs) else { return false }
        let range = NSRange(location: 0, length: lhs.utf16.count)
        return regex.firstMatch(in: lhs, options: [], range: range) != nil
    }
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

extension RTCRtpTransceiverDirection {
    
    func toString() -> String {
        switch self {
        case .inactive:
            return "inactive"
        case .sendRecv:
            return "sendRecv"
        case .sendOnly:
            return "sendOnly"
        case .recvOnly:
            return "recvOnly"
        case .stopped:
            return "stopped"
        @unknown default:
            return "unknown"
        }
    }
    
}

extension RTCRtpMediaType {
    
    func toString() -> String {
        switch self {
        case .audio:
            return "audio"
        case .video:
            return "video"
        case .data:
            return "data"
        @unknown default:
            return "unknown"
        }
    }
    
}
