//
//  Consumer.swift
//  WebRTCTest
//
//  Created by xcodvv on 26.06.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import WebRTC

protocol TopicDelegate {
    func topicPeerState(topic: Topic, peer: PeerConnection)
    func topicChannelState(topic: Topic, channel: PeerDataChannel, peer: PeerConnection)
    func topicMessage(topic: Topic, msg: Message, channel: PeerDataChannel, peer: PeerConnection, resolve: ()->Void)
    func topicMessage(topic: Topic, msg: Message, resolve: ()->Void)
}

class Topic: NSObject, MessageChannelDelegate {
    var name: String
    var delegates = MulticastDelegate<TopicDelegate>()
    var peers = Dictionary<String, PeerConnection>()
    private var channels = [PeerDataChannel]()
    
    init(_ name: String) {
        self.name = name
        super.init()
    }
    
    func removeChannel(_ ch: PeerDataChannel) {
        
    }
    
    func addChannel(_ ch: PeerDataChannel, _ peer: PeerConnection) {
        if self.peers[peer.endpoint] == nil {
            self.peers[peer.endpoint] = peer
            self.delegates.invoke { delegate in
                delegate.topicPeerState(topic: self, peer: peer)
            }
        }
        if !self.channels.contains(ch) {
            self.channels.append(ch)
            self.delegates.invoke { delegate in
                delegate.topicChannelState(topic: self, channel: ch, peer: peer)
            }
            ch.delegates.add(self)
        }
    }
    
    func send(_ data: Dictionary<String, Any?>) {
        for ch in channels {
            ch.send(data)
        }
    }
    
    func channelConnectionError(channel: MessageChannel, error: Error) {
        print(error)
    }
    
    func channelConnectionState(channel: MessageChannel, state: MessageChannelState) {
        switch state {
        case .closed:
            self.removeChannel(channel as! PeerDataChannel)
        default:
            return
        }
    }
    
    func channelMessage(channel: MessageChannel, msg: Message, resolve: () -> Void) {
        if let ch = channel as? PeerDataChannel {
            self.delegates.invoke { delegate in
                delegate.topicMessage(topic: self, msg: msg, channel: ch, peer: ch.peer, resolve: resolve)
            }
        } else {
            self.delegates.invoke { delegate in
                delegate.topicMessage(topic: self, msg: msg, resolve: resolve)
            }
        }
    }
}
