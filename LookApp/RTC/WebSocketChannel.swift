//
//  WebSocketChannel.swift
//  LookApp
//
//  Created by xcodvv on 30.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import Promises

class WebSocketChannel: MessageChannel {
    
    var endpoint: String = ""
    var subProtocol: String = ""
    var socket = WebSocket()
    
    override init() {
        super.init()
        enable()
    }
    
    func open(_ endpoint: String, subProtocol: String)->Promise<MessageChannel> {
        self.endpoint = endpoint
        self.subProtocol = subProtocol
        return super.open()
    }
    
    override internal func connect() {
        if _readyState != .connecting {
            _readyState = .connecting
            delegates.invoke { delegate in
                delegate.channelConnectionState(channel: self, state: .connecting)
            }
            socket.open(endpoint, subProtocols: [ subProtocol ])
        }
    }
    
    override func send(_ message: String) {
        ready.then { _ in
            self.socket.send(text: message)
        }
    }
    
    override func send(_ msg: Dictionary<String, Any?>) {
        ready.then { _ in
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: msg)
                self.socket.send(data: jsonData)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func enable() {
        socket.event.open = {
            self.openAttempt?.fulfill(self)
            self.ready.fulfill(self)
            if self._readyState != .open {
                self._readyState = .open
                self.delegates.invoke { delegate in
                    delegate.channelConnectionState(channel: self, state: .open)
                }
            }
        }
        
        socket.event.close = { code, reason, clean in
            //print("[ws] close \(code) \(reason) \(clean)")
            self.openAttempt = nil
            if  self._readyState != .closed {
                self._readyState  = .closed
                self.delegates.invoke { delegate in
                    delegate.channelConnectionState(channel: self, state: .closed)
                }
            }
        }
        
        socket.event.error = { error in
            //print("[ws] error \(error)")
            self.openAttempt?.reject(error)
            self.delegates.invoke { delegate in
                delegate.channelConnectionError(channel: self, error: error)
            }
        }
        
        socket.event.message = { message in
            var msg = message as! String
            let err = self.handleData(msg.data(using: .utf8)!)
            switch err {
            case .unexpectedMethod:
                print("[ws]", self.endpoint, "unexpected method", msg)
            case .unexpectedMessage:
                print("[ws]", self.endpoint, "unexpected message", msg)
            case .unexpectedData:
                print("[ws]", self.endpoint, "unexpected data", msg)
            default:
                return
            }
        }
    }
    
}
