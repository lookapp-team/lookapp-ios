//
//  iceServers.swift
//  WebRTCTest
//
//  Created by xcodvv on 02.07.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import WebRTC

class IceConfig {
    static let servers: [RTCIceServer] = [
        RTCIceServer(urlStrings: ["stun:stun.l.google.com:19302"]),
//        RTCIceServer(urlStrings: ["stun:stun1.l.google.com:19302"]),
//        RTCIceServer(urlStrings: ["stun:stun2.l.google.com:19302"]),
//        RTCIceServer(urlStrings: ["stun:stun3.l.google.com:19302"]),
//        RTCIceServer(urlStrings: ["stun:stun4.l.google.com:19302"]),
//        RTCIceServer(urlStrings: ["stun:turn.openmvc.ru:3478"]),
//        RTCIceServer(urlStrings: ["stun:global.stun.twilio.com:3478"]),
//        RTCIceServer(urlStrings: ["stun:stun.1und1.de:3478"]),
//        RTCIceServer(urlStrings: ["stun:stun.gmx.net:3478"]),
//        RTCIceServer(urlStrings: [
//            "turn:relay.instant.io:443?transport=udp",
//            "turn:relay.instant.io:443?transport=tcp",
//            "turns:relay.instant.io:443?transport=tcp"
//        ], username:"relay.instant.io", credential:"nepal-cheddar-baize-oleander" ),
        RTCIceServer(urlStrings: ["turn:turn.openmvc.ru:3478"], username: "xcodvv88", credential: "impala64")
    ]
}
