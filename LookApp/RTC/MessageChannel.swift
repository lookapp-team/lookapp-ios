//
//  MessageChannel.swift
//  LookApp
//
//  Created by xcodvv on 30.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import Promises

typealias MessageFullfillMethod = ()->Void
typealias HandleMethod = (Message)->Void

enum MessageChannelState {
    case new
    case connecting
    case open
    case closed
}

enum MessageChannelError: Error {
    case unexpectedMessage
    case unexpectedMethod
    case unexpectedData
}

protocol MessageChannelDelegate: class {
    func channelConnectionError(channel: MessageChannel, error: Error)
    func channelConnectionState(channel: MessageChannel, state: MessageChannelState)
    func channelMessage(channel: MessageChannel, msg: Message, resolve: ()->Void)
}

class MessageChannel: NSObject {
    var ready: Promise<MessageChannel>
    var openAttempt: Promise<MessageChannel>?
    var delegates = MulticastDelegate<MessageChannelDelegate>()
    private var seq: Int = 0
    private var pending: Dictionary<String, Promise<JSON>>
    private var handler: Dictionary<String, [HandleMethod]>
    
    internal var _readyState: MessageChannelState
    var readyState: MessageChannelState {
        return self._readyState
    }
    
    override init() {
        self._readyState = .new
        self.ready = Promise<MessageChannel>.pending()
        self.pending = Dictionary<String, Promise<JSON>>()
        self.handler = Dictionary<String, [HandleMethod]>()
        super.init()
    }
    
    internal func connect() {
        
    }
    
    func open() -> Promise<MessageChannel> {
        if self.openAttempt==nil {
            self.openAttempt = Promise<MessageChannel>.pending()
            self.connect()
        }
        return self.ready
    }
    
    func send(_ message: String) {
        
    }
    
    func send(_ data: Dictionary<String, Any?>) {
        
    }
    
    func call(_ method: String, _ params: Dictionary<String, Any?>)->Promise<JSON> {
        self.seq += 1
        let id = self.seq
        let dfd = Promise<JSON>.pending()
        self.pending[String(id)] = dfd
        self.send([ "id":String(id), "method": method, "params":params ])
        return dfd
    }
    
    func call(_ method: String)->Promise<JSON> {
        self.seq += 1
        let id = self.seq
        let dfd = Promise<JSON>.pending()
        self.pending[String(id)] = dfd
        self.send([ "id":String(id), "method": method, "params":[] ])
        return dfd
    }
    
    func handle(_ method: String, _ callback: @escaping HandleMethod) {
        if var hh = self.handler[method] {
            hh.append(callback)
        } else {
            self.handler[method] = [callback]
        }
    }
    
    internal func handleData(_ data: Data)->(MessageChannelError?) {
        do {
            let msg = try Message.from(data, [])
            var isFullfilled = false
            let fullfill = { ()->Void in isFullfilled = true }
            self.delegates.invoke { delegate in
                delegate.channelMessage(channel: self, msg: msg, resolve: fullfill)
            }
            if !isFullfilled && msg.contains("method") {
                if !isFullfilled {
                    let method = msg.key("method")!
                    if let hh = self.handler[method] {
                        isFullfilled = true
                        for h in hh {
                            h(msg)
                        }
                    } else {
                        return .unexpectedMethod
                    }
                }
                return nil
            }
            if !isFullfilled && msg.contains("id") {
                let id = msg.key("id")!
                if let pending = self.pending[id] {
                    isFullfilled = true
                    pending.fulfill(msg.toJSON("result")!)
                } else {
                    return .unexpectedMessage
                }
                return nil
            }
            if !isFullfilled {
                return .unexpectedData
            }
        } catch {
            print(error.localizedDescription)
            return .unexpectedData
        }
        return nil
    }
}
