//
//  Signaler.swift
//  LookApp
//
//  Created by xcodvv on 30.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import Promises
import WebRTC
import SwiftUI

enum SignalerError: Error {
    case endpointUnreachable
    case timeout
}

enum SignalerState {
    case new
    case connecting
    case open
    case close
}

enum SignalerMessageType: String {
    case consumer
    case offer
    case accept
    case ice
    case wellcome
}

protocol SignalerDelegate {
    
    func didAddPeer(peer: PeerConnection)
    
    func shouldCreateOffer(for peer: PeerConnection)
    
    func willCreateAnswer(for peer: PeerConnection)
    
    // didRemovePeer
    // didOpenDataChannel
    // didCloseDataChannel
}

class Signaler: NSObject, ObservableObject {
    
    @Published private(set) var readyState: SignalerState = .new
    
    var delegates = MulticastDelegate<SignalerDelegate>()
    var pending: Bool = false
    var channel: MessageChannel? = nil
    var ready = Promise<Signaler>.pending()
    var peers = Dictionary<String, PeerConnection>()
    var peerChannel = Dictionary<String, PeerDataChannel>()
    var topics = Dictionary<String, Topic>()
    var selfAddress: String = ""
    
    let defaultPeerConnectionConfig: RTCConfiguration = {
        let conf = RTCConfiguration()
        conf.sdpSemantics = .unifiedPlan
        conf.iceServers = IceConfig.servers
        conf.continualGatheringPolicy = .gatherContinually
        //conf.allowCodecSwitching
        return conf
    }()
    
    let defaultPeerDataChannelConfig: RTCDataChannelConfiguration = {
        let conf = RTCDataChannelConfiguration()
        conf.channelId = 420 //conf.protocol = "live"
        conf.isOrdered = true
        conf.isNegotiated = true
        conf.maxRetransmits = 3
        //conf.maxPacketLifeTime = 30000
        return conf
    }()
    
    func createProducer(_ topicName: String) -> Topic? {
        guard let ch = channel else { return nil }
        if topics[topicName] == nil {
            topics[topicName] = Topic(topicName)
            let _ = ch.call("producer", [ "topic":topicName ])
        }
        return topics[topicName]
    }
    
    func subscribe(_ topicName: String) -> Topic? {
        guard let ch = channel else { return nil }
        if topics[topicName] == nil {
            topics[topicName] = Topic(topicName)
            let _ = ch.call("subscribe", [ "topic":topicName ])
        }
        return topics[topicName]
    }
    
    func setChannel(_ ch: MessageChannel) {
        if channel != nil {
            channel!.delegates.remove(self)
        }
        channel = ch
        ch.delegates.add(self)
        if ch.readyState == MessageChannelState.open {
            channelConnectionState(channel: ch, state: .open)
        }
    }
    
    func handleConsumerMessage(_ msg: Message) {
        guard let params = msg.params() else { return }
        guard let endpoint = params.toString("endpoint") else { return }
        //guard let topicName = params.toString("topic") else { return }
        
        guard peers[endpoint] == nil else { return }
        
        print("[signaler] got peer", endpoint)
        
        let peer = PeerConnection(endpoint, signaler: self)
        peer.awake(config: defaultPeerConnectionConfig)
        
        //guard let rtc = peer.rtc else { return }
        
        peers[endpoint] = peer
        peer.negotiable = false
        
        delegates.invoke { delegate in
            delegate.didAddPeer(peer: peer)
        }
        
//        guard let topicChannel = rtc.dataChannel(forLabel: topicName, configuration: defaultPeerDataChannelConfig) else { return }
        
//        let peerChannel = PeerDataChannel(topicChannel, peer)
//        let _ = peerChannel.open()
//        peer.dataChannel[topicName] = peerChannel
//        self.peerChannel[endpoint] = peerChannel
//        peerChannel.ready.then { c in
//            print("[peer] open channel \(peerChannel.name) \(endpoint)")
//            if let topic = self.topics[topicName] {
//                topic.addChannel(peerChannel, peer)
//            }
//            peer.setLogLevel(.none)
//        }
        
        peer.negotiable = true
        
        delegates.invoke { delegate in
            delegate.shouldCreateOffer(for: peer)
        }
        
    }

    func handleOfferMessage(_ msg: Message) {
        guard let params = msg.params() else { return }
        guard let endpoint = params.toString("endpoint") else { return }
        guard let sdp = params.toString("sdp") else { return }
        
        guard peers[endpoint] == nil else {
            return handleRenegotiationMessage(msg)
        }
        
        print("[signaler] got remote offer", endpoint)
        
        let peer = PeerConnection(endpoint, signaler: self)
        peer.negotiable = false
        peer.awake(config: defaultPeerConnectionConfig)
        
        peers[endpoint] = peer
        
        delegates.invoke { delegate in
            delegate.didAddPeer(peer: peer)
        }
        
        let passOffer = Promise<PeerConnection>.pending()
        let passAnswer = Promise<RTCSessionDescription>.pending()
        
        let desc = RTCSessionDescription(type: .offer, sdp: sdp)
        peer.rtc.setRemoteDescription(desc, completionHandler: { error in
            if error == nil {
                print("[peer] remote description set")
                passOffer.fulfill(peer)
            } else {
                print("[peer] remote description error: \(error!)")
                passOffer.reject(error!)
            }
        })
        
        passOffer.then { peer in
        
            self.delegates.invoke { delegate in
                delegate.willCreateAnswer(for: peer)
            }
            
//            for topic in self.topics {
//                if let topicChannel = peer.rtc.dataChannel(forLabel: topic.value.name, configuration: self.defaultPeerDataChannelConfig) {
//                    let peerChannel = PeerDataChannel(topicChannel, peer)
//                    let _ = peerChannel.open()
//                    peer.dataChannel[topic.value.name] = peerChannel
//                    self.peerChannel[endpoint] = peerChannel
//                    peerChannel.ready.then { c in
//                        print("[peer] open channel \(peerChannel.name) \(endpoint)")
//                        topic.value.addChannel(peerChannel, peer)
//                        peer.setLogLevel(.none)
//                    }
//                }
//            }
            
            peer.rtc.answer(for: peer.constraints, completionHandler: { desc, error in
                if error == nil && desc != nil {
                    peer.rtc.setLocalDescription(desc!, completionHandler: { error in
                        passAnswer.fulfill(desc!)
                    })
                }
            })
        }
        
        passAnswer.then { desc in
            self.channel!.send([ "id": msg.id, "result": [ "sdp": desc.sdp ] ])
        }
    }
    
    func handleHaveRemoteOfferState() {
        
    }
    
    func handleRenegotiationMessage(_ msg: Message) {
        guard let params = msg.params() else { return }
        guard let endpoint = params.toString("endpoint") else { return }
        guard let sdp = params.toString("sdp") else { return }
        
        guard let peer = peers[endpoint] else { return }
        
        print("[signaler] got remote offer to renegotiation", endpoint)
        
        let passOffer = Promise<PeerConnection>.pending()
        let passAnswer = Promise<RTCSessionDescription>.pending()
        
        let desc = RTCSessionDescription(type: .offer, sdp: sdp)
        peer.rtc.setRemoteDescription(desc, completionHandler: { error in
            if error == nil {
                print("[peer] remote description set")
                passOffer.fulfill(peer)
            } else {
                print("[peer] remote description error: \(error!)")
                passOffer.reject(error!)
            }
        })
        
        passOffer.then { peer in
            peer.rtc.answer(for: peer.constraints, completionHandler: { desc, error in
                if error == nil && desc != nil {
                    peer.rtc.setLocalDescription(desc!, completionHandler: { error in
                        passAnswer.fulfill(desc!)
                    })
                }
            })
        }
        
        passAnswer.then { desc in
            self.channel!.send([ "id": msg.id, "result":[ "sdp": desc.sdp ] ])
        }
    }
    
    func handleAcceptMessage(_ msg: Message) {
        guard let params = msg.params() else { return }
        guard let endpoint = params.toString("endpoint") else { return }
        guard let peer = peers[endpoint] else { return }
        peer.accept.fulfill(peer)
        //peer.rtc.setLocalDescription(, completionHandler: )
    }
    
    func handleICEMessage(_ msg: Message) {
        guard let params = msg.params() else { return }
        guard let endpoint = params.toString("endpoint") else { return }
        guard let candidate = params.toJSON("candidate") else { return }
        guard let sdpMid = candidate.toString("sdpMid") else { return }
        guard let sdpMLineIndex = candidate.toInt32("sdpMLineIndex") else { return }
        guard let peer = peers[endpoint] else { return }
        let iceCandidate = RTCIceCandidate(sdp: candidate.toString("candidate", ""),
                                           sdpMLineIndex: sdpMLineIndex,
                                           sdpMid: sdpMid)
        peer.rtc.add(iceCandidate)
    }
}

extension Signaler: MessageChannelDelegate {
    
    func channelConnectionState(channel: MessageChannel, state: MessageChannelState) {
        switch(state) {
        case .connecting:
            pending = true
            withAnimation {
                self.readyState = .connecting
            }
            
        case .open:
            if pending {
                pending = false
                withAnimation {
                    self.readyState = .open
                }
                ready.fulfill(self)
            }
            
        case .new, .closed:
            if !pending {
                withAnimation {
                    self.readyState = .close
                }
            }
        }
    }
    
    func channelConnectionError(channel: MessageChannel, error: Error) {
        
    }
    
    func channelMessage(channel: MessageChannel, msg: Message, resolve: () -> Void) {
        let method = msg.toString("method", "")
        switch(SignalerMessageType(rawValue: method)) {
        case .consumer:
            handleConsumerMessage(msg)
        case .offer:
            handleOfferMessage(msg)
        case .accept:
            handleAcceptMessage(msg)
        case .ice:
            handleICEMessage(msg)
        case .wellcome:
            if let p = msg.params() {
                self.selfAddress = p.toString("endpoint", "")
                if self.selfAddress.count > 0 {
                    print("[peer] local endpoint", selfAddress)
                }
            }
        default:
            return
        }
        resolve()
    }
    
}
