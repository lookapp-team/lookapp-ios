//
//  PeerConnectonDelegate.swift
//  LookApp
//
//  Created by xcodvv on 30.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import Promises
import WebRTC

extension PeerConnection: RTCPeerConnectionDelegate {
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        guard negotiable else { return }
        
        print("[ice] peer", endpoint, "should negotiate connection")
        
        createOffer().then { desc in
            if let ch = self.signaler.channel {
                self.sendOffer(desc, viaChannel: ch)
            } else {
                print("[ch] can`t find peer data channel", self.endpoint)
            }
        }
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didStartReceivingOn transceiver: RTCRtpTransceiver) {
        delegates.invoke { delegate in
            delegate.didStartReceiving(on: transceiver, peer: self)
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd rtpReceiver: RTCRtpReceiver, streams mediaStreams: [RTCMediaStream]) {
        delegates.invoke { delegate in
            delegate.didAddReceiver(rtpReceiver: rtpReceiver, streams: mediaStreams, peer: self)
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove rtpReceiver: RTCRtpReceiver) {
        delegates.invoke { delegate in
            delegate.didRemoveReceiver(rtpReceiver: rtpReceiver, peer: self)
        }
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
//        print("[peer] got data channel: ", dataChannel.label)
//        if let ch = self.dataChannel[dataChannel.label] {
//            ch.setDataChannel(dataChannel: dataChannel)
//        }
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        let exp = NSRegularExpression("^candidate:([0-9]+)")
        if let cid = exp.first(candidate.sdp) {
            print("[peer]", endpoint, candidate.sdp)
            accept.then {_ in
                print("[ice]", cid, "via signaler")
                self.sendICECandidate(candidate, viaChannel: self.signaler.channel!)
            }
            ice.fulfill(self)
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        var state = ""
        switch newState {
        case .new:
            state = "new"
        case .gathering:
            state = "gathering"
        case .complete:
            state = "complete"
        @unknown default:
            return
        }
        print("[peer]", endpoint, "RTCIceGatheringState: \(state)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        var state = ""
        switch newState {
        case .connected:
            state = "connected"
        case .completed:
            state = "completed"
        case .disconnected:
            state = "disconnected"
        case .failed:
            state = "failed"
        case .checking:
            state = "checking"
        case .closed:
            state = "closed"
        case .new:
            state = "new"
        case .count:
            state = "count"
        @unknown default:
            return
        }
        print("[peer]", endpoint, "RTCIceConnectionState: \(state)")
        delegates.invoke { delegate in
            delegate.didChangeIceConnectionState(newState: newState, peer: self)
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        var state = ""
        switch stateChanged {
        case .stable:
            state = "stable"
        case .haveLocalOffer:
            state = "haveLocalOffer"
        case .haveRemoteOffer:
            state = "haveRemoteOffer"
        case .haveLocalPrAnswer:
            state = "haveLocalPrAnswer"
        case .haveRemotePrAnswer:
            state = "haveRemotePrAnswer"
        case .closed:
            state = "closed"
        @unknown default:
            return
        }
        print("[peer]", endpoint, "RTCSignalingState: \(state)")
    }
    
}
