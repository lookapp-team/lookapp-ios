//
//  PeerDataChannel.swift
//  LookApp
//
//  Created by xcodvv on 31.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import Promises
import WebRTC

class PeerDataChannel: MessageChannel {
    
    let name: String
    var peer: PeerConnection
    var dataChannel: RTCDataChannel
    
    init(_ dataChannel: RTCDataChannel, _ peer: PeerConnection) {
        self.dataChannel = dataChannel
        self.name = dataChannel.label
        self.peer = peer
        super.init()
        dataChannel.delegate = self
    }
    
    func setDataChannel(dataChannel: RTCDataChannel) {
        dataChannel.delegate = self
        self.dataChannel = dataChannel
    }
    
    override internal func connect() {
        switch dataChannel.readyState {
        case .connecting:
            self._readyState = .connecting
            self.delegates.invoke { delegate in
                delegate.channelConnectionState(channel: self, state: .connecting)
            }
            break;
        case .open:
            self._readyState = .connecting
            self.delegates.invoke { delegate in
                delegate.channelConnectionState(channel: self, state: .connecting)
            }
            self.ready.fulfill(self)
            self._readyState = .open
            self.delegates.invoke { delegate in
                delegate.channelConnectionState(channel: self, state: .open)
            }
        default:
            break;
        }
    }
    
    override func send(_ message: String) {
        ready.then({ ch in
            let buf = RTCDataBuffer(data: message.data(using: .utf8)!, isBinary: false)
            self.dataChannel.sendData(buf)
        })
    }
    
    override func send(_ data: Dictionary<String, Any?>) {
        ready.then({ ch in
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: data)
                let buf = RTCDataBuffer(data: jsonData, isBinary: true)
                self.dataChannel.sendData(buf)
            } catch {
                print(error.localizedDescription)
            }
        })
    }
}

extension PeerDataChannel: RTCDataChannelDelegate {
    
    func dataChannelDidChangeState(_ dataChannel: RTCDataChannel) {
        switch(dataChannel.readyState) {
        case .open:
            // print("channel \(dataChannel.label) is opened")
            self.ready.fulfill(self)
            if self._readyState != .open {
                self._readyState = .open
                self.delegates.invoke { delegate in
                    delegate.channelConnectionState(channel: self, state: .open)
                }
            }
            break;
        case .connecting:
            if  self._readyState != .connecting {
                self._readyState = .connecting
                self.delegates.invoke { delegate in
                    delegate.channelConnectionState(channel: self, state: .connecting)
                }
            }
            break;
        case .closing:
            break;
        case .closed:
            print("channel \(dataChannel.label) is closed")
            self.openAttempt = nil
            if  self._readyState != .closed {
                self._readyState  = .closed
                self.delegates.invoke { delegate in
                    delegate.channelConnectionState(channel: self, state: .closed)
                }
            }
            break;
        default:
            break;
        }
    }
    
    func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        let err = self.handleData(buffer.data)
        switch err {
        case .unexpectedMethod:
            print("[ch]", self.name, peer.endpoint, "unexpected method", String(decoding: buffer.data, as: UTF8.self))
        case .unexpectedMessage:
            print("[ch]", self.name, peer.endpoint, "unexpected message", String(decoding: buffer.data, as: UTF8.self))
        case .unexpectedData:
            print("[ch]", self.name, peer.endpoint, "unexpected data", String(decoding: buffer.data, as: UTF8.self))
        default:
            return
        }
    }
    
}
