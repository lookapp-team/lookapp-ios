//
//  PeerConnection.swift
//  LookApp
//
//  Created by xcodvv on 30.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import Promises
import WebRTC

protocol PeerConnectionDelegate: class {
    
    func didStartReceiving(on transceiver: RTCRtpTransceiver, peer: PeerConnection)
    
    func didAddReceiver(rtpReceiver: RTCRtpReceiver, streams mediaStreams: [RTCMediaStream], peer: PeerConnection)
    
    func didRemoveReceiver(rtpReceiver: RTCRtpReceiver, peer: PeerConnection)
    
    func didChangeIceConnectionState(newState: RTCIceConnectionState, peer: PeerConnection)

}

class PeerConnectonFactory {
    
    static let videoEncoderFactory = RTCDefaultVideoEncoderFactory()
    
    static let videoDecoderFactory = RTCDefaultVideoDecoderFactory()
    
    static let sharedFactory: RTCPeerConnectionFactory = {
        //RTCInitializeSSL()
        for codec in videoEncoderFactory.supportedCodecs() {
            print("[codec] video", codec.name, codec.parameters)
        }
        return RTCPeerConnectionFactory(encoderFactory: videoEncoderFactory, decoderFactory: videoDecoderFactory)
    }()
    
}

class PeerConnection: NSObject {
    
    var delegates = MulticastDelegate<PeerConnectionDelegate>()
    
    var rtc: RTCPeerConnection!
    var constraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
    var endpoint: String
    var signaler: Signaler
    
    var negotiable = false
    var restartIce = false
    
    var orientation = UIScreen.main
    var role = ConferenceRole.speaker
    
    var accept = Promise<PeerConnection>.pending()
    var ice = Promise<PeerConnection>.pending()
    
    let videoSource = PeerConnectonFactory.sharedFactory.videoSource()
    
    //var dataChannelReady = Promise<P2PChannel>.pending()
    //var dataChannel = Dictionary<String, PeerDataChannel>()

    
    init(_ endpoint: String, signaler: Signaler) {
        self.endpoint = endpoint
        self.signaler = signaler
        super.init()
        //videoSource.adaptOutputFormat(toWidth: 480, height: 360, fps: 30)
    }
    
    func setLogLevel(_ logLevel: RTCLoggingSeverity) {
        RTCSetMinDebugLogLevel(logLevel)
    }
    
    func awake(config: RTCConfiguration) {
        rtc = PeerConnectonFactory.sharedFactory.peerConnection(with: config, constraints: self.constraints, delegate: self)
    }
     
    func createOffer() -> Promise<RTCSessionDescription> {
        let pending = Promise<RTCSessionDescription>.pending()
        
        if restartIce {
            constraints = RTCMediaConstraints(
                mandatoryConstraints: [
                    kRTCMediaConstraintsIceRestart: kRTCMediaConstraintsValueTrue,
                ],
                optionalConstraints: nil
            )
        }
        
        rtc.offer(for: constraints) { (desc, error) in
            if let desc = desc {
                print("[ice] offer", self.endpoint, "created")
                self.rtc.setLocalDescription(desc, completionHandler: { (error) in
                    if error != nil {
                        print("[peer]", self.endpoint, "local description got error: \(error!)")
                        return;
                    }
                    print("[peer]", self.endpoint, "local description set")
                    pending.fulfill(desc)
                })
            }
        }
        return pending
    }
    
    func sendOffer(_ desc: RTCSessionDescription, viaChannel ch: MessageChannel) {
        ch.call("offer", [ "endpoint":self.endpoint, "sdp":desc.sdp ]).then { r in
            let desc = RTCSessionDescription(type: .answer, sdp: r.toString("sdp", ""))
            self.rtc.setRemoteDescription(desc) { (error) in
                print("[peer]", self.endpoint, "got remote description")
                if error == nil {
                    print("[peer]", self.endpoint, "remote description set")
                    ch.send([ "method":"accept", "params":["endpoint":self.endpoint] ])
                    DispatchQueue.main.async {
                        self.accept.fulfill(self)
                    }
                } else {
                    print("[peer]", self.endpoint, "got error: \(error!)")
                }
            }
        }
    }
    
    func sendICECandidate(_ candidate: RTCIceCandidate, viaChannel ch: MessageChannel) {
        ch.send([
            "method": "ice",
            "params": [
                "endpoint": endpoint,
                "candidate":[  "candidate":candidate.sdp, "sdpMid":candidate.sdpMid!, "sdpMLineIndex":candidate.sdpMLineIndex ]
            ]
        ])
    }
    
}
