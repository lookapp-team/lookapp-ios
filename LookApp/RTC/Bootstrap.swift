//
//  Bootstrapper.swift
//  LookApp
//
//  Created by xcodvv on 30.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation
import Promises
import WebRTC
import SwiftUI

class Bootstrap: NSObject, ObservableObject, MessageChannelDelegate  {
    
    @Published private(set) var readyState: SignalerState = .new
    
    var endpoint: String = ""
    var subProtocol: String = ""
    var pending: Bool = false
    var totalAttempts: Int = 0
    var maxAttempts: Int = 3
    let channel = WebSocketChannel()
    var ready = Promise<Bootstrap>.pending()
    
    override init() {
        super.init()
        channel.delegates.add(self)
    }
    
    func open(_ endpoint: String, _ subProtocol: String) {
        totalAttempts = 0
        ready = Promise<Bootstrap>.pending()
        self.endpoint = endpoint
        self.subProtocol = subProtocol
        let _ = self.channel.open(endpoint, subProtocol: subProtocol)
    }
    
    func channelConnectionState(channel: MessageChannel, state: MessageChannelState) {
        switch(state) {
        case .connecting:
            pending = true
            withAnimation {
                self.readyState = .connecting
            }
            
        case .open:
            print("[bootstrap] \(self.channel.endpoint) connected")
            if pending {
                pending = false
                withAnimation {
                    self.readyState = .open
                }
                ready.fulfill(self)
            }
            
        case .new, .closed:
            if pending {
                totalAttempts += 1
                if self.maxAttempts < 0 || self.totalAttempts < self.maxAttempts {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0)  {
                        let _ = self.channel.open(self.endpoint, subProtocol: self.subProtocol)
                    }
                } else {
                    ready.reject(SignalerError.endpointUnreachable)
                }
            } else {
                withAnimation {
                    self.readyState = .close
                }
            }
        }
    }
    
    func channelConnectionError(channel: MessageChannel, error: Error) {
        print("[bootstrap] error: \(error.localizedDescription)")
        channelConnectionState(channel: channel, state: .closed)
    }
    
    func channelMessage(channel: MessageChannel, msg: Message, resolve: () -> Void) {
        
    }
    
}
