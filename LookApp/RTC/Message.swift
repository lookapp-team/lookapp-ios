//
//  Message.swift
//  LookApp
//
//  Created by xcodvv on 30.08.2020.
//  Copyright © 2020 xcodvv. All rights reserved.
//

import Foundation

class Message: JSON {
    static func from(_ data: Data, _ options: JSONSerialization.ReadingOptions) throws ->Message {
        do {
            let data = try JSONSerialization.jsonObject(with: data, options: options)
            return Message(data as! RawObject)
        } catch {
            throw error
        }
    }
    
    func params()->JSON? {
        if let val = self.data["params"] {
            return JSON(val as! RawObject)
        }
        return nil
    }
    
    func commit(_ channel: MessageChannel) {
        channel.send([ "id": id, "result": [ "ok":true ] ])
    }
    
    func commit(_ channel: MessageChannel, _ data:  Dictionary<String, Any?>) {
        channel.send([ "id": id, "result": data ])
    }
    
    var id: String {
        if let val = key("id") {
            return val
        }
        return ""
    }
}
